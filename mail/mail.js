require('dotenv').config()
const pug = require('pug')
const juice = require('juice')
const htmlToText = require('html-to-text')
const mailjet = require('node-mailjet').connect(
  process.env.SMTP_PUBLIC,
  process.env.SMTP_PRIVATE
)

const generateHTML = (filename, options = {}) => {
  // const f = `${__dirname}/../../mail/templates/${filename}.pug`
  const html = pug.renderFile(`${__dirname}/templates/${filename}.pug`, options)
  const inlined = juice(html)
  return inlined
}

exports.send = async (options) => {
  try {
    const html = generateHTML(options.filename, options)
    const text = htmlToText.fromString(html)

    const mailOptions = {
      FromEmail: process.env.SMTP_FROM_EMAIL,
      FromName: process.env.SMTP_FROM_NAME,
      Subject: options.subject,
      'Text-part': text,
      'Html-part': html,
      Recipients: [{Email: options.to}],
    }

    const request = await mailjet.post('send').request(mailOptions)
    console.log(request.body)
    return request.body
  } catch (error) {
    console.log(error)
  }
}
