const express = require('express');
const auth = require('../controllers/admin/auth');
const profile = require('../controllers/admin/profile');
const settings = require('../controllers/general/settings');
const sectors = require('../controllers/general/org-sectors');
const report = require('../controllers/admin/report');
const ice = require('../controllers/user/ice');
const notification = require('../controllers/admin/notification');
const messaging = require('../controllers/admin/messaging');
const sandbox = require('../utils/promise-sandbox');
const {
	tokenMiddleware,
	adminMiddleware,
	activeAdminMiddleware
} = require('../config/middleware');

const router = express.Router();

router.post('/login', sandbox(auth.login));

router.post('/requsest-reset-password', sandbox(auth.requestResetPassword));

router.post('/reset-password', sandbox(auth.resetPassword));

// require token for subsequent endpoints
router.use(tokenMiddleware);

// allow inactive admin for this endpoint
router.post(
	'/change-password',
	sandbox(adminMiddleware),
	sandbox(auth.changePassword)
);

// allow only active admins for subsequent endpoints
router.use(sandbox(activeAdminMiddleware));
router.post('/add-admin', sandbox(auth.addNewAdmin));
router.post('/suspend-admin/:adminId', sandbox(auth.suspendAdmin));
router.post('/reactivate-admin/:adminId', sandbox(auth.reActivateAdmin));

router.get('/profile', sandbox(profile.getProfile));
router.put('/profile', sandbox(profile.updateProfile));
router.put('/profile/picture', sandbox(profile.updateProfilePicture));

router.put('/config', sandbox(settings.updateSettings));
router.delete('/config', sandbox(settings.deleteSettings));

router.post('/org-sectors', sandbox(sectors.addSectors));
router.delete('/org-sectors', sandbox(sectors.deleteSectors));

router.patch('/ice/pause/:iceId', sandbox(ice.pauseIce));
router.get('/ice/get-user-ices/:userId', sandbox(report.getUserICEs));
router.patch('/ice/unpause/:iceId', sandbox(ice.unPauseIce));

router.post('/suspend-user/:userId', sandbox(auth.suspendUser));
router.post('/reactivate-user/:userId', sandbox(auth.reActivateUser));
router.post('/org', sandbox(auth.createOrganization));

router.get('/report/ice', sandbox(report.getICEs));
router.get('/report/organizations', sandbox(report.getOrganizations));
router.get('/report/users', sandbox(report.getUsers));
router.get('/report/dashboard', sandbox(report.getDashboard));
router.get('/report/admins', sandbox(report.getAdmins));
router.get('/report/transactions', sandbox(report.getTransactions));
router.get('/report/distress-calls', sandbox(report.getDistressCallHistory));
router.patch(
	'/report/distress-calls',
	sandbox(report.updateDistressCallStatus)
);

router.put('/notification-id', sandbox(notification.registerFCMID));
router.post('/message', sandbox(messaging.sendMessage));

module.exports = router;
