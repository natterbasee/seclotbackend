const express = require('express');
const auth = require('../controllers/user/auth');
const profile = require('../controllers/user/profile');
const distress = require('../controllers/user/distress');
const notification = require('../controllers/user/notification');
const ice = require('../controllers/user/ice');
const wallet = require('../controllers/user/wallet');
const subscription = require('../controllers/user/subscription');
const sandbox = require('../utils/promise-sandbox');
const {requireAuthCode, userMiddleware} = require('../config/middleware');

const router = express.Router();

router.post('/login', sandbox(auth.login));
router.get('/otp', sandbox(auth.requestOTP));
router.get('/forgot-password', sandbox(auth.forgotPassowrd));
router.get('/resend-otp', sandbox(auth.resendOTP));
router.get('/verify-otp', sandbox(auth.verifyOTP));

// these endpoints require authCode
router.post('/register', requireAuthCode, sandbox(auth.register));
router.post('/set-pin', requireAuthCode, sandbox(auth.setPIN));

// require authorization token for subsequent endpoints

router.get('/profile', userMiddleware, sandbox(profile.getProfile));
router.put('/profile', userMiddleware, sandbox(profile.updateProfile));
router.put(
  '/profile/picture',
  userMiddleware,
  sandbox(profile.updateProfilePicture)
);
router.put(
  '/profile/subscription',
  userMiddleware,
  sandbox(subscription.updateSubscriptionPlan)
);

router.put(
  '/notification-id',
  userMiddleware,
  sandbox(notification.registerFCMID)
);

router.get(
  '/distress-call',
  userMiddleware,
  sandbox(distress.distressCallsHistory)
);
router.post(
  '/distress-call',
  userMiddleware,
  sandbox(distress.issueDistressCall)
);

router.get('/ice', userMiddleware, sandbox(ice.getICEs));
router.post('/ice', userMiddleware, sandbox(ice.addICE));
router.put('/ice/:iceId', userMiddleware, sandbox(ice.updateICE));
router.delete('/ice/:iceId', userMiddleware, sandbox(ice.removeICE));

router.post('/fund-wallet', userMiddleware, sandbox(wallet.fundWallet));
router.get('/get-wallet', userMiddleware, sandbox(wallet.walletBalance));

module.exports = router;
