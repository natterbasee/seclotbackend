const adminRoutes = require('./admin');
const organizationRoutes = require('./organization');
const userRoutes = require('./user');
const otherRoutes = require('./others');

module.exports = {
	adminRoutes,
	organizationRoutes,
	userRoutes,
	otherRoutes
};
