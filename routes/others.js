const express = require("express");
const settings = require("../controllers/general/settings");
const sectors = require("../controllers/general/org-sectors");
const auth = require("../controllers/general/auth");
const sandbox = require("../utils/promise-sandbox");

const router = express.Router();

router.get("/config", sandbox(settings.getSettings));
router.get("/org-sectors", sandbox(sectors.getSectors));

router.get("/auth/request-password-reset", sandbox(auth.requestPasswordReset));
router.post("/auth/reset-password", sandbox(auth.resetPassword));
router.post("/auth/refresh-token", sandbox(auth.refreshToken));

module.exports = router;
