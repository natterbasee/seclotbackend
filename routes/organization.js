const express = require('express')
const auth = require('../controllers/organization/auth')
const profile = require('../controllers/organization/profile')
const subscription = require('../controllers/organization/subscription')
const ice = require('../controllers/organization/ice')
const beneficiary = require('../controllers/organization/beneficiary')
const wallet = require('../controllers/organization/wallet')
const notification = require('../controllers/organization/notification')
const messaging = require('../controllers/organization/messaging')
const report = require('../controllers/organization/report')
const sandbox = require('../utils/promise-sandbox')
const {
  tokenMiddleware,
  organizationMiddleware,
} = require('../config/middleware')

const router = express.Router()

router.post('/login', sandbox(auth.login))
router.post('/register', sandbox(auth.register))
router.get('/confirm-account/:token', sandbox(auth.confirm))
// require token for subsequent endpoints
router.use(tokenMiddleware)
// allow only organizations for subsequent endpoints
router.use(sandbox(organizationMiddleware))

router.get('/profile', sandbox(profile.getProfile))
router.put('/profile', sandbox(profile.updateProfile))
router.put('/profile/picture', sandbox(profile.updateProfilePicture))
router.put(
  '/profile/subscription',
  sandbox(subscription.updateSubscriptionPlan)
)

router.get('/ice', sandbox(ice.getICEs))
router.post('/ice', sandbox(ice.addICE))
router.put('/ice/:iceId', sandbox(ice.updateICE))
router.delete('/ice/:iceId', sandbox(ice.removeICE))
router.patch('/ice/pause/:iceId', sandbox(ice.pauseIce))
router.patch('/ice/unpause/:iceId', sandbox(ice.unPauseIce))

router.get('/beneficiaries', sandbox(beneficiary.getBeneficiaries))
router.post('/beneficiaries', sandbox(beneficiary.addBeneficiaries))
router.put('/beneficiaries', sandbox(beneficiary.updateBeneficiaryICE))
router.patch('/beneficiaries', sandbox(beneficiary.enableOrDisableBeneficiary))

router.post('/fund-wallet', sandbox(wallet.fundWallet))
router.put('/notification-id', sandbox(notification.registerFCMID))
router.post('/test-notification', sandbox(notification.sendNotificationToApp))

router.post('/message', sandbox(messaging.sendMessage))

router.get('/report/ice', sandbox(report.iceReport))
router.get('/report/wallet', sandbox(report.walletReport))
router.get('/report/wallet/pdf', sandbox(report.walletReportPDF))
router.get('/report/beneficiaries', sandbox(report.beneficiaryReport))
router.get('/report/dashboard', sandbox(report.dashboardReport))

module.exports = router
