const FirebaseAdmin = require("firebase-admin");
const multer = require("multer");
const { join, extname } = require("path");
const { unlinkSync } = require("fs");
const { bindTo, execSilentCallback, silentPromise } = require("silent-promise");
const utils = require("../../utils");
const constants = require("../../utils/constants");
const logger = require("../../utils/logger");

async function getProfile(req, res) {
    res.is.ok(utils.getFormattedProfile(req.admin, req));
}

async function updateProfile(req, res) {
    const updateFields = ["name", "phoneNumber", "email", "password"];
    if (utils.hasNoneOfTheRequiredParameters(req.body, updateFields, res)) {
        return;
    }

    if (req.body.password) {
        if (req.body.password.length < 6) {
            res.is.badRequest(400, "Password must be 6 or more characters");
            return;
        }
        if (utils.passwordIsNotValid(req.body.password)) {
            res.is.badRequest(400, utils.passwordErrorMessage);
            return;
        }
    }

    if (req.body.email || req.body.password) {
        const newInfo = {
            email: req.body.email,
            password: req.body.password
        };
        const [firebaseError] = await silentPromise(FirebaseAdmin.auth().updateUser(req.admin.id, newInfo));
        if (firebaseError.getError()) {
            res.is.badRequest(400, firebaseError.getError().message);
            return;
        }
    }

    const update = {};
    for (const field of updateFields) {
        if (req.body[field] && field !== "password") {
            update[field] = req.body[field];
        }
    }

    if (Object.keys(update) === 0) {
        // nothing else to update, it's safe to return the profile in req.admin
        res.is.ok(utils.getFormattedProfile(req.admin, req));
        return;
    }

    // update other fields passed in
    const updateAdminProfile = { $set: update };
    const updateOptions = { returnOriginal: false };

    const query = { _id: req.admin.id };
    const db = require("../../config/db").getDatabase();
    const adminCollection = db.collection(constants.database.collections.admins);
    const [updateAdminInfoError, updateResult] =
        await execSilentCallback(adminCollection.findOneAndUpdate, bindTo(adminCollection))(query, updateAdminProfile, updateOptions);
    updateAdminInfoError.ifError("Failed to update admin profile on database").thenStopExecution();

    res.is.ok(utils.getFormattedProfile(updateResult.value, req));
}

async function updateProfilePicture(req, res) {
    const [uploadError] = await execSilentCallback(getMulterInstance().single("image"), req, res);
    const error = uploadError.getError();
    if (error) {
        switch (error.code) {
            case "LIMIT_UNEXPECTED_FILE":
                res.is.badRequest(400, `Expected 'image' field, got '${error.field}'`);
                break;

            default:
                logger.error({ detail: error, path: req.path, method: req.method }, "Profile picture upload error");
                res.is.serverError("Error processing file");
                break;
        }
        return;
    }

    if (!req.file) {
        return res.is.badRequest(400, "Missing file parameter: image");
    }

    const updateAdminProfile = { $set: { picture: join(`${process.env.SERVER}/image/${req.file.filename}`) } };
    const updateOptions = { returnOriginal: false };

    const query = { _id: req.admin.id };
    const db = require("../../config/db").getDatabase();
    const adminCollection = db.collection(constants.database.collections.admins);
    const [updateAdminInfoError, updateResult] =
        await execSilentCallback(adminCollection.findOneAndUpdate, bindTo(adminCollection))(query, updateAdminProfile, updateOptions);
    updateAdminInfoError.ifError("Failed to update admin profile on database").thenStopExecution();

    // delete previous image file
    // const previousProfilePictureFileName = req.admin.picture;
    // if (previousProfilePictureFileName) {
    //     const path = join(previousProfilePictureFileName);
    //     unlinkSync(path);
    // }

    res.is.ok(utils.getFormattedProfile(updateResult.value, req));
}

function getMulterInstance() {
    const profileImageStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(undefined, process.env.UPLOAD_PATH);
        },
        filename: (req, file, cb) => {
            cb(undefined, `profile_image_${Date.now()}${extname(file.originalname)}`);
        }
    });
    const imageFilter = (req, file, cb) => {
        cb(undefined, file.mimetype.startsWith("image"));
    };

    return multer({ storage: profileImageStorage, fileFilter: imageFilter, limits: { fieldSize: 2 } });
}

module.exports = {
    getProfile,
    updateProfile,
    updateProfilePicture
};