const FirebaseAuth = require('firebaseauth');
const FirebaseAdmin = require('firebase-admin');
const {bindTo, execSilentCallback, silentPromise} = require('silent-promise');
const utils = require('../../utils');
const constants = require('../../utils/constants');
const {send} = require('../../mail/mail');
const {sendSMS} = require('../general/messaging');
const crypto = require('crypto');
const {getNextId} = require('../general/auth');
const {sendNewSignupNotificationToAdmin} = require('./notification');

async function login(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['email', 'password'], res)) {
		return;
	}
	req.body.email = req.body.email.toLowerCase();

	const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
	utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
	const [authError, authResult] = await execSilentCallback(
		firebaseAuth.signInWithEmail,
		req.body.email,
		req.body.password
	);
	if (authError.getError()) {
		res.is.badRequest(401, authError.getError().message);
		return;
	}

	const findById = {_id: authResult.user.id};

	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const [findUserError, admin] = await execSilentCallback(
		adminCollection.findOne,
		bindTo(adminCollection)
	)(findById);
	findUserError
		.ifError('Failed to get admin info from database')
		.thenStopExecution();

	if (!admin) {
		res.is.badRequest(403, 'You\'re NOT an admin');
	} else if (admin.suspended) {
		res.is.badRequest(403, 'You have been suspended as an Admin');
	} else if (!admin.active) {
		res.is.ok({
			mustSetPassword: true,
			token: authResult.token
		});
	} else {
		res.is.ok({
			profile: utils.getFormattedProfile(admin, req),
			refreshToken: authResult.refreshToken,
			token: authResult.token,
			tokenExpiry: authResult.expiryMilliseconds
		});
	}
}

async function addNewAdmin(req, res) {
	if (
		utils.parametersAreIncomplete(req.body, ['name', 'email', 'password'], res)
	) {
		return;
	}
	req.body.email = req.body.email.toLowerCase();

	const query = {email: req.body.email};
	const options = {projection: {active: true}};

	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const [findUserError, admin] = await execSilentCallback(
		adminCollection.findOne,
		bindTo(adminCollection)
	)(query, options);
	findUserError
		.ifError('Failed to get admin info from database')
		.thenStopExecution();

	if (admin) {
		res.is.badRequest(400, 'Email already in use');
		return;
	}

	// register on firebase, then save to database
	const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
	utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
	const [authError, authResult] = await execSilentCallback(
		firebaseAuth.registerWithEmail,
		req.body.email,
		req.body.password,
		req.body.name
	);
	if (authError.getError()) {
		res.is.badRequest(401, authError.getError().message);
		return;
	}
	const newAdminData = {
		_id: authResult.user.id,
		name: req.body.name,
		email: req.body.email,
		active: false,
		createdBy: req.user.id,
		accountCreationDate: Date.now()
	};
	const [saveAdminInfoError] = await execSilentCallback(
		adminCollection.insertOne,
		bindTo(adminCollection)
	)(newAdminData);
	saveAdminInfoError
		.ifError('Failed to save new admin info to database')
		.thenStopExecution();

	await send({
		subject: 'You are an Admin',
		email: req.body.email,
		name: req.body.name,
		password: req.body.password,
		to: req.body.email,
		resetPasswordLink: 'http://seclot.natterbase.com/forgot-password',
		activationLink: 'http://seclot.natterbase.com/login',
		hostname: 'seclot.natterbase.com',
		filename: 'activation-admin'
	}).catch(err => {
		console.log(err);
	});

	return res.is.ok({message: 'Success. New admin can proceed to login'});
}

async function requestResetPassword(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['email'], res)) {
		return;
	}
	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const [findUserError, admin] = await execSilentCallback(
		adminCollection.findOne,
		bindTo(adminCollection)
	)({email: req.body.email});
	findUserError
		.ifError('Failed to find admin from database')
		.thenStopExecution();

	if (admin) {
		const query = {_id: admin._id};
		const token = crypto.randomBytes(32).toString('hex');
		const updateAdminProfile = {
			$set: {resetPasswordToken: token}
		};
		const [updateAdminInfoError] = await execSilentCallback(
			adminCollection.updateOne,
			bindTo(adminCollection)
		)(query, updateAdminProfile, {new: true});
		updateAdminInfoError
			.ifError('Failed to update admin status on database')
			.thenStopExecution();

		await send({
			subject: 'We heard you need to reset your password!',
			email: admin.email,
			name: admin.name,
			to: admin.email,
			resetLink: `http://seclot.natterbase.com/admin/reset-password?token=${token}`,
			hostname: 'seclot.natterbase.com',
			filename: 'password-reset-request'
		}).catch(err => {
			console.log(err);
		});

		return res.is.ok({message: 'A reset password email has been sent to you!'});
	}

	res.is.badRequest(404, 'This email does not exist!');
}

async function resetPassword(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['password', 'token'], res)) {
		return;
	}
	if (req.body.password.length < 6) {
		res.is.badRequest(400, 'Password must be 6 or more characters');
		return;
	}
	if (utils.passwordIsNotValid(req.body.password)) {
		res.is.badRequest(400, utils.passwordErrorMessage);
		return;
	}
	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const [findUserError, admin] = await execSilentCallback(
		adminCollection.findOne,
		bindTo(adminCollection)
	)({resetPasswordToken: req.body.token});
	findUserError
		.ifError('Failed to find admin from database')
		.thenStopExecution();
	if (admin) {
		const [firebaseError] = await silentPromise(
			FirebaseAdmin.auth().updateUser(admin._id, {password: req.body.password})
		);
		firebaseError.ifError('Update admin password failed').thenStopExecution();
		const query = {_id: admin._id};
		const updateAdminProfile = {$set: {resetPasswordToken: ''}};
		const [updateAdminInfoError] = await execSilentCallback(
			adminCollection.updateOne,
			bindTo(adminCollection)
		)(query, updateAdminProfile);
		updateAdminInfoError
			.ifError('Failed to update admin status on database')
			.thenStopExecution();

		await send({
			subject: 'You password has been reset!',
			name: admin.name,
			to: admin.email,
			hostname: 'seclot.natterbase.com',
			filename: 'password-success'
		}).catch(err => {
			console.log(err);
		});

		return res.is.ok({message: 'Your password has been reset successfully!'});
	}
	res.is.badRequest(404, 'This email does not exist!');
}

async function changePassword(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['password'], res)) {
		return;
	}

	if (req.body.password.length < 6) {
		res.is.badRequest(400, 'Password must be 6 or more characters');
		return;
	}
	if (utils.passwordIsNotValid(req.body.password)) {
		res.is.badRequest(400, utils.passwordErrorMessage);
		return;
	}

	// update password on firebase
	const [firebaseError] = await silentPromise(
		FirebaseAdmin.auth().updateUser(req.admin.id, {password: req.body.password})
	);
	firebaseError.ifError('Update admin password failed').thenStopExecution();

	if (!req.admin.active) {
		await markAdminAccountAsActive(req.admin.id);
		req.admin.active = true;
	}

	// get new token and refresh token
	const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
	utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
	const [authError, authResult] = await execSilentCallback(
		firebaseAuth.signInWithEmail,
		req.admin.email,
		req.body.password
	);
	if (authError.getError()) {
		res.is.badRequest(401, authError.getError().message);
		return;
	}

	res.is.ok({
		refreshToken: authResult.refreshToken,
		token: authResult.token,
		tokenExpiry: authResult.expiryMilliseconds
	});
}

async function suspendUser(req, res) {
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const query = {_id: req.params.userId};
	const [saveError, user] = await execSilentCallback(
		usersCollection.updateOne,
		bindTo(usersCollection)
	)(query, {$set: {suspended: true, suspendedBy: req.user.id}}, {new: true});
	saveError.ifError('Failed suspend a user').thenStopExecution();
	if (user) {
		const [findError, foundUser] = await execSilentCallback(
			usersCollection.findOne,
			bindTo(usersCollection)
		)(query);
		//send sms
		await sendSMS(
			foundUser._id,
			`You have been suspend from the Seclot platform. Contact ${req.admin.email} to find out why!`
		);
		return res.is.ok({message: 'You have suspended user successfully!'});
	}
	res.is.badRequest(404, 'This user does not exist!');
}

async function reActivateUser(req, res) {
	console.log(req.admin);
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const query = {_id: req.params.userId};
	const [saveError, user] = await execSilentCallback(
		usersCollection.updateOne,
		bindTo(usersCollection)
	)(query, {$set: {suspended: false, reactivatedBy: req.user.id}}, {new: true});
	saveError.ifError('Failed suspend a user').thenStopExecution();
	if (user) {
		const [findError, foundUser] = await execSilentCallback(
			usersCollection.findOne,
			bindTo(usersCollection)
		)(query);
		await sendSMS(
			foundUser._id,
			`You have been suspend from the Seclot platform. Contact ${req.admin.email} to find out why!`
		);
		return res.is.ok({message: 'You have re-activated user successfully!'});
	}
	res.is.badRequest(404, 'This user does not exist!');
}

async function suspendAdmin(req, res) {
	if (!req.admin.userStatus === 'SUPER') {
		res.is.badRequest(404, 'You are not a Super Admin');
	}
	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const query = {_id: req.params.adminId};
	const [saveError, user] = await execSilentCallback(
		adminCollection.updateOne,
		bindTo(adminCollection)
	)(query, {$set: {suspended: true, suspendedBy: req.user.id}}, {new: true});
	saveError.ifError('Failed suspend a user').thenStopExecution();
	if (user) {
		const [findError, foundUser] = await execSilentCallback(
			adminCollection.findOne,
			bindTo(adminCollection)
		)(query);

		await send({
			subject: 'You have been Suspended',
			email: req.admin.email,
			name: foundUser.name,
			to: foundUser.email,
			login: 'http://seclot.natterbase.com/login',
			hostname: 'seclot.natterbase.com',
			filename: 'suspend-user'
		}).catch(err => {
			console.log(err);
		});
		return res.is.ok({message: 'You have suspended Admin successfully!'});
	}
	res.is.badRequest(404, 'This Admin does not exist!');
}

async function reActivateAdmin(req, res) {
	if (!req.admin.userStatus === 'SUPER') {
		res.is.badRequest(404, 'You are not a Super Admin');
	}
	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const query = {_id: req.params.adminId};
	const [saveError, user] = await execSilentCallback(
		adminCollection.updateOne,
		bindTo(adminCollection)
	)(query, {$set: {suspended: false, reactivatedBy: req.user.id}}, {new: true});
	saveError.ifError('Failed suspend a user').thenStopExecution();
	if (user) {
		const [findError, foundUser] = await execSilentCallback(
			adminCollection.findOne,
			bindTo(adminCollection)
		)(query);
		await send({
			subject: 'You have been Activated',
			email: req.admin.email,
			name: foundUser.name,
			to: foundUser.email,
			login: 'http://seclot.natterbase.com/login',
			hostname: 'seclot.natterbase.com',
			filename: 'activate-user'
		}).catch(err => {
			console.log(err);
		});
		return res.is.ok({message: 'You have re-activated admin successfully!'});
	}
	res.is.badRequest(404, 'This user does not exist!');
}

async function createOrganization(req, res) {
	if (
		utils.parametersAreIncomplete(
			req.body,
			['name', 'email', 'password', 'phoneNumber', 'location'],
			res
		)
	) {
		return;
	}
	req.body.email = req.body.email.toLowerCase();

	if (req.body.password.length < 6) {
		res.is.badRequest(400, 'Password must be 6 or more characters');
		return;
	}
	if (utils.passwordIsNotValid(req.body.password)) {
		res.is.badRequest(400, utils.passwordErrorMessage);
		return;
	}

	if (req.body.phoneNumber.length !== 11) {
		res.is.badRequest(400, 'Phone number should be 11 digits');
		return;
	}
	if (req.body.phoneNumber.split().some(digit => isNaN(digit))) {
		res.is.badRequest(400, 'Phone number is invalid');
		return;
	}

	const db = require('../../config/db').getDatabase();
	const orgCollection = db.collection(
		constants.database.collections.organizations
	);
	const duplicateFields = await getDuplicatedFields(
		['name', 'email', 'phoneNumber'],
		req.body,
		orgCollection
	);

	if (duplicateFields.length > 0) {
		res.is.badRequest(
			400,
			`An organization already exists with the same ${duplicateFields.join(
				', '
			)}`
		);
		return;
	}

	// register on firebase, then save to database
	const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
	utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
	const [authError, authResult] = await execSilentCallback(
		firebaseAuth.registerWithEmail,
		req.body.email,
		req.body.password,
		req.body.name
	);
	if (authError.getError()) {
		res.is.badRequest(401, authError.getError().message);
		return;
	}
	const newSeclotId = await getNextId();
	const newOrgInfo = {
		_id: authResult.user.id,
		name: req.body.name,
		email: req.body.email,
		phoneNumber: req.body.phoneNumber,
		location: req.body.location,
		seclotId: newSeclotId,
		createdBy: req.user.id,
		accountCreationDate: Date.now()
	};
	const [saveOrgInfoError] = await execSilentCallback(
		orgCollection.insertOne,
		bindTo(orgCollection)
	)(newOrgInfo);
	saveOrgInfoError
		.ifError('Failed to save new organization info to database')
		.thenStopExecution();
	sendNewSignupNotificationToAdmin(
		newOrgInfo.name,
		newOrgInfo.phoneNumber,
		'organization'
	);

	await send({
		subject: 'Welcome to Seclot',
		email: req.body.email,
		name: req.body.name,
		password: req.body.password,
		to: req.body.email,
		activationLink: 'http://seclot.natterbase.com/login',
		hostname: 'seclot.natterbase.com',
		filename: 'new-org'
	}).catch(err => {
		console.log(err);
	});

	return res.is.ok({message: 'You have created an organzation successfully!'});
}

async function markAdminAccountAsActive(userId) {
	const query = {_id: userId};
	const updateAdminProfile = {$set: {active: true}};
	const db = require('../../config/db').getDatabase();
	const adminCollection = db.collection(constants.database.collections.admins);
	const [updateAdminInfoError] = await execSilentCallback(
		adminCollection.updateOne,
		bindTo(adminCollection)
	)(query, updateAdminProfile);
	updateAdminInfoError
		.ifError('Failed to update admin status on database')
		.thenStopExecution();
}

async function getDuplicatedFields(
	fieldsToCheckForDuplicates,
	data,
	orgCollection
) {
	const checkFieldsForDuplicatesArray = [];
	const options = {projection: {}};

	for (const field of fieldsToCheckForDuplicates) {
		options.projection[field] = true;

		const fieldCheck = {};
		fieldCheck[field] = data[field];
		checkFieldsForDuplicatesArray.push(fieldCheck);
	}

	const duplicateQuery = {$or: checkFieldsForDuplicatesArray};
	const [findError, organization] = await execSilentCallback(
		orgCollection.findOne,
		bindTo(orgCollection)
	)(duplicateQuery, options);
	findError
		.ifError('Error checking registration info for duplicated info')
		.thenStopExecution();

	const duplicateFields = [];

	if (organization) {
		for (const field of fieldsToCheckForDuplicates) {
			if (organization[field] === data[field]) {
				duplicateFields.push(field);
			}
		}
	}

	return duplicateFields;
}

module.exports = {
	login,
	suspendAdmin,
	reActivateAdmin,
	createOrganization,
	suspendUser,
	addNewAdmin,
	changePassword,
	requestResetPassword,
	resetPassword,
	reActivateUser
};
