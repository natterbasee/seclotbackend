const {bindTo, execSilentCallback} = require('silent-promise');
const {getAllBeneficiaries} = require('../organization/beneficiary');
const constants = require('../../utils/constants');
const utils = require('../../utils');

async function getOrganizations(req, res) {
	const page = req.query.page || 1,
		limit = req.query.limit || 20;
	const skip = (page - 1) * limit;
	const sort = {};
	if (req.query.sort) {
		sort[req.query.sort] = req.query.sortOrder === 'asc' ? 1 : -1;
	} else {
		sort.accountCreationDate = 1; // sort with accountCreationDate asc, by default
	}

	const db = require('../../config/db').getDatabase();
	const orgCollection = db.collection(
		constants.database.collections.organizations
	);

	const findOptions = {
		projection: {beneficiaries: false, disabledBeneficiaries: false}
	};
	const [error, cursor] = await execSilentCallback(
		orgCollection.find,
		bindTo(orgCollection)
	)({}, findOptions);
	error.ifError('Error fetching all organizations info').thenStopExecution();

	let organizations = await cursor
		.sort(sort)
		.skip(skip)
		.limit(+limit)
		.toArray();
	organizations = organizations.map(org => {
		org.id = org._id;
		delete org._id;
		return org;
	});

	const [, count] = await execSilentCallback(
		orgCollection.countDocuments,
		bindTo(orgCollection)
	)({});

	const result = {organizations, page};
	if (count) {
		result.pages = Math.ceil(count / limit);
	}

	res.is.ok(result);
}

async function getICEs(req, res) {
	const page = req.query.page || 1,
		limit = req.query.limit || 20;
	const skip = (page - 1) * limit;
	const sort = {};
	if (req.query.sort) {
		sort[req.query.sort] = req.query.sortOrder === 'asc' ? 1 : -1;
	} else {
		sort.dateAdded = 1; // sort with dateAdded asc, by default
	}

	const query = {};
	if (req.query.organizationId) {
		query.addedBy = {
			accountId: req.query.organizationId,
			accountType: 'organization'
		};
	} else if (req.query.userId) {
		query.addedBy = {
			accountId: req.query.userId,
			accountType: 'user'
		};
	}

	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);

	const [, count] = await execSilentCallback(
		iceCollection.countDocuments,
		bindTo(iceCollection)
	)(query);
	const [error, cursor] = await execSilentCallback(
		iceCollection.find,
		bindTo(iceCollection)
	)(query);
	error.ifError('Error fetching ICEs').thenStopExecution();

	const accountNames = {},
		ICEs = [];
	const selectiveCursor = cursor
		.sort(sort)
		.skip(skip)
		.limit(+limit);

	while (await selectiveCursor.hasNext()) {
		const ice = await selectiveCursor.next();

		// get info for added by
		let accountName =
      accountNames[`${ice.addedBy.accountType}_${ice.addedBy.accountId}`];
		if (!accountName) {
			accountName = await getAccountName(
				db,
				ice.addedBy.accountType,
				ice.addedBy.accountId
			);
			accountNames[
				`${ice.addedBy.accountType}_${ice.addedBy.accountId}`
			] = accountName;
		}

		ice.addedBy.accountName = accountName;
		ice.id = ice._id;
		delete ice._id;

		ICEs.push(ice);
	}

	const result = {ICEs, page};
	if (count) {
		result.pages = Math.ceil(count / limit);
	}

	res.is.ok(result);
}

async function getAccountName(db, accountType, accountId) {
	const collectionName =
    accountType === 'user'
    	? constants.database.collections.users
    	: constants.database.collections.organizations;
	const collection = db.collection(collectionName);
	const query = {_id: accountId};
	const options = {projection: {name: true, firstName: true, lastName: true}};

	const [error, account] = await execSilentCallback(
		collection.findOne,
		bindTo(collection)
	)(query, options);
	error.ifError(`Error getting name of ${accountType}`).thenStopExecution();

	return account
		? account.name || `${account.firstName} ${account.lastName}`
		: accountType;
}

async function getUsers(req, res) {
	if (req.query.organizationId) {
		const beneficiaries = await getAllBeneficiaries(req.query.organizationId);
		res.is.ok({users: beneficiaries, page: 1, pages: 1});
		return;
	}

	const page = req.query.page || 1,
		limit = req.query.limit || 20;
	const skip = (page - 1) * limit;
	const sort = {};
	if (req.query.sort) {
		sort[req.query.sort] = req.query.sortOrder === 'asc' ? 1 : -1;
	} else {
		sort.accountCreationDate = 1; // sort with accountCreationDate asc, by default
	}

	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);

	const findOptions = {projection: {pinHash: false, notificationIds: false}};
	const [error, cursor] = await execSilentCallback(
		usersCollection.find,
		bindTo(usersCollection)
	)({}, findOptions);
	error.ifError('Error fetching all users data').thenStopExecution();

	let users = await cursor
		.sort(sort)
		.skip(skip)
		.limit(+limit)
		.toArray();
	users = users.map(user => {
		user.phoneNumber = user._id;
		delete user._id;

		if (user.location) {
			user.lastKnownLocation = {
				longitude: user.location.coordinates[0],
				latitude: user.location.coordinates[1]
			};
			delete user.location;
		}

		return user;
	});

	const [, count] = await execSilentCallback(
		usersCollection.countDocuments,
		bindTo(usersCollection)
	)({});

	const result = {users, page};
	if (count) {
		result.pages = Math.ceil(count / limit);
	}

	res.is.ok(result);
}

async function getDistressCallHistory(req, res) {
	// allow admin connect with the respective ICEs that were sent a distress message and subsequently mark the distress call as resolved
	const page = req.query.page || 1,
		limit = req.query.limit || 20;
	const skip = (page - 1) * limit;
	const sort = {};
	if (req.query.sort) {
		sort[req.query.sort] = req.query.sortOrder === 'asc' ? 1 : -1;
	} else {
		sort.callDate = 1; // sort with callDate asc, by default
	}

	const query = {};
	if (req.query.status) {
		query.status = req.query.status;
	}
	if (req.query.organizationId) {
		query.organizationsNotified = req.query.organizationId;
	}
	if (req.query.userId) {
		query.userId = req.query.userId;
	}

	const db = require('../../config/db').getDatabase();
	const distressCollection = db.collection(
		constants.database.collections.distressCalls
	);
	const [error, cursor] = await execSilentCallback(
		distressCollection.find,
		bindTo(distressCollection)
	)(query);
	error.ifError('Error getting distress call history').thenStopExecution();
	const history = await cursor
		.sort(sort)
		.skip(skip)
		.limit(+limit)
		.toArray();

	// get names of each user
	const userIds = new Set();
	for (const call of history) {
		userIds.add(call.userId);
	}

	const usersCollection = db.collection(constants.database.collections.users);
	const userQuery = {_id: {$in: Array.from(userIds)}};
	const options = {projection: {firstName: true, lastName: true}};
	const [getUsersError, usersCursor] = await execSilentCallback(
		usersCollection.find,
		bindTo(usersCollection)
	)(userQuery, options);
	getUsersError
		.ifError('Error getting user info for distress calls')
		.thenStopExecution();

	const users = await usersCursor.toArray();
	const userNames = {};
	for (const user of users) {
		userNames[user._id] = `${user.firstName} ${user.lastName}`;
	}

	const distressCalls = history.map(call => {
		call.userName = userNames[call.userId];
		call.organizationsNotified = call.organizationsNotified.length;
		call.icesNotified = call.icesNotified.length;
		call.id = call._id;
		delete call._id;
		return call;
	});

	const [, count] = await execSilentCallback(
		distressCollection.countDocuments,
		bindTo(distressCollection)
	)(query);

	const result = {distressCalls, page};
	if (count) {
		result.pages = Math.ceil(count / limit);
	}

	res.is.ok(result);
}

async function updateDistressCallStatus(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['callIds', 'status'], res)) {
		return;
	}

	const distressCallIds = req.body.callIds;
	if (!(distressCallIds instanceof Array)) {
		res.is.badRequest(
			400,
			`callIds should be array, got ${typeof distressCallIds}`
		);
		return;
	}

	const allowedStatuses = ['pending', 'settled', 'failed'];
	const status = req.body.status.toLowerCase();
	if (allowedStatuses.indexOf(status) < 0) {
		res.is.badRequest(
			400,
			`callIds should be one of ${allowedStatuses.join(', ')}. Got ${status}`
		);
		return;
	}

	const query = {
		_id: {$in: distressCallIds}
	};
	const update = {
		$set: {status}
	};

	const db = require('../../config/db').getDatabase();
	const distressCollection = db.collection(
		constants.database.collections.distressCalls
	);
	const [updateError] = await execSilentCallback(
		distressCollection.updateMany,
		bindTo(distressCollection)
	)(query, update);
	updateError
		.ifError('Failed to update distress call status')
		.thenStopExecution();

	res.is.ok({success: true, message: 'Updated successfully'});
}

async function getTransactions(req, res) {
	const txTypes = ['debit', 'credit'];
	if (req.query.type && txTypes.indexOf(req.query.type.toLowerCase()) < 0) {
		res.is.badRequest(400, `type must be one of ${txTypes.join(', ')}`);
		return;
	}

	const page = req.query.page || 1,
		limit = req.query.limit || 20;
	const skip = (page - 1) * limit;
	const sort = {};
	if (req.query.sort) {
		sort[req.query.sort] = req.query.sortOrder === 'asc' ? 1 : -1;
	} else {
		sort.paymentDate = 1; // sort with paymentDate asc, by default
	}

	const query = {};
	if (req.query.type) {
		query.txType = req.query.type.toLowerCase();
	}
	if (req.query.organizationId) {
		query.entity = {
			id: req.query.organizationId,
			type: 'organization'
		};
	} else if (req.query.userId) {
		query.entity = {
			id: req.query.userId,
			type: 'user'
		};
	}
	const options = {projection: {_id: false, processedDate: false}};

	const db = require('../../config/db').getDatabase();
	const txCollection = db.collection(
		constants.database.collections.transactions
	);
	const [error, transactionsCursor] = await execSilentCallback(
		txCollection.find,
		bindTo(txCollection)
	)(query, options);
	error.ifError('Error retrieving transaction records').thenStopExecution();

	const transactions = await transactionsCursor
		.sort(sort)
		.skip(skip)
		.limit(+limit)
		.toArray();
	const accountNames = {};

	for (const tx of transactions) {
		// get info for entity
		let accountName = accountNames[`${tx.entity.type}_${tx.entity.id}`];
		if (!accountName) {
			accountName = await getAccountName(db, tx.entity.type, tx.entity.id);
			accountNames[`${tx.entity.type}_${tx.entity.id}`] = accountName;
		}

		tx.account = tx.entity;
		tx.account.name = accountName;
		delete tx.entity;
	}

	const [, count] = await execSilentCallback(
		txCollection.countDocuments,
		bindTo(txCollection)
	)(query);

	const result = {transactions, page};
	if (count) {
		result.pages = Math.ceil(count / limit);
	}

	res.is.ok(result);
}

async function getAdmins(req, res) {
	const page = req.query.page || 1,
		limit = req.query.limit || 20;
	const skip = (page - 1) * limit;
	const sort = {};
	if (req.query.sort) {
		sort[req.query.sort] = req.query.sortOrder === 'asc' ? 1 : -1;
	} else {
		sort.accountCreationDate = 1; // sort with accountCreationDate asc, by default
	}

	const db = require('../../config/db').getDatabase();
	const adminsCollection = db.collection(constants.database.collections.admins);

	const findOptions = {projection: {pinHash: false, notificationIds: false}};
	const [error, cursor] = await execSilentCallback(
		adminsCollection.find,
		bindTo(adminsCollection)
	)({}, findOptions);
	error.ifError('Error fetching all admins data').thenStopExecution();

	let admins = await cursor
		.sort(sort)
		.skip(skip)
		.limit(+limit)
		.toArray();
	admins = admins.map(user => {
		user.phoneNumber = user._id;
		delete user._id;

		if (user.location) {
			user.lastKnownLocation = {
				longitude: user.location.coordinates[0],
				latitude: user.location.coordinates[1]
			};
			delete user.location;
		}

		return user;
	});

	const [, count] = await execSilentCallback(
		adminsCollection.countDocuments,
		bindTo(adminsCollection)
	)({});

	const result = {admins, page};
	if (count) {
		result.pages = Math.ceil(count / limit);
	}

	res.is.ok(result);
}

async function getDashboard(req, res) {
	const db = require('../../config/db').getDatabase();
	const orgCollection = db.collection(
		constants.database.collections.organizations
	);
	const usersCollection = db.collection(constants.database.collections.users);
	const distressCollection = db.collection(
		constants.database.collections.distressCalls
	);

	const [, orgCount] = await execSilentCallback(
		orgCollection.countDocuments,
		bindTo(orgCollection)
	)({});
	const [, userCount] = await execSilentCallback(
		usersCollection.countDocuments,
		bindTo(usersCollection)
	)({});
	const [, distressCount] = await execSilentCallback(
		distressCollection.countDocuments,
		bindTo(distressCollection)
	)({});

	res.is.ok({
		orgCount,
		userCount,
		distressCount
	});
}

async function getUserICEs(req, res) {
	if (utils.parametersAreIncomplete(req.params, ['userId'], res)) {
		return;
	}
	let query = {
		'addedBy.accountId': req.params.userId,
		'addedBy.accountType': 'user'
	};
	const options = {projection: {addedBy: false}};

	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	let findError, cursor;

	[findError, cursor] = await execSilentCallback(
		iceCollection.find,
		bindTo(iceCollection)
	)(query, options);
	findError
		.ifError('failed to get user ICEs from database')
		.thenStopExecution();

	let userICEs = await cursor.toArray();
	userICEs = userICEs.map(ice => {
		ice.id = ice._id;
		delete ice._id;
		return ice;
	});

	// get ICEs assigned to user by organization
	const userICEsCollection = db.collection(
		constants.database.collections.user_ices
	);
	query = {userId: req.params.userId};
	options.projection = {status: true, iceID: true, assignDate: true};
	[findError, cursor] = await execSilentCallback(
		userICEsCollection.find,
		bindTo(userICEsCollection)
	)(query, options);
	findError
		.ifError('failed to get organization-assigned ICEs from database')
		.thenStopExecution();

	const orgCollection = db.collection(
		constants.database.collections.organizations
	);
	const organizationICEs = [];
	while (await cursor.hasNext()) {
		const iceLink = await cursor.next();

		// get ice info using ice id
		query = {_id: iceLink.iceID};
		options.projection = {name: true, addedBy: true};
		const [getICEError, ice] = await execSilentCallback(
			iceCollection.findOne,
			bindTo(iceCollection)
		)(query, options);
		getICEError.ifError('Error reading ICE info').thenStopExecution();

		if (!ice) {
			continue;
		}

		// get organization name
		query = {_id: ice.addedBy.accountId};
		options.projection = {name: true};
		const [getOrgError, org] = await execSilentCallback(
			orgCollection.findOne,
			bindTo(orgCollection)
		)(query, options);
		getOrgError
			.ifError('Error reading super organization info')
			.thenStopExecution();

		ice.organization = org.name;
		ice.paused = iceLink.status === 'paused';
		ice.assignDate = iceLink.assignDate;
		ice.id = ice._id;
		delete ice._id;
		delete ice.addedBy;

		organizationICEs.push(ice);
	}

	res.is.ok({userICEs, organizationICEs});
}

module.exports = {
	getOrganizations,
	getICEs,
	getUsers,
	getUserICEs,
	getDashboard,
	getAdmins,
	getDistressCallHistory,
	updateDistressCallStatus,
	getTransactions
};
