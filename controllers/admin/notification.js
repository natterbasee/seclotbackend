const { bindTo, execSilentCallback } = require("silent-promise");
const { sendNotificationToApp } = require("../general/notifications");
const utils = require("../../utils");
const constants = require("../../utils/constants");

async function registerFCMID(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["notificationId"], res)) {
        return;
    }
    if (typeof req.body.notificationId !== "string") {
        res.is.badRequest(400, "Invalid notificationId");
        return;
    }

    const query = { _id: req.admin.id };
    const update = {
        $addToSet: { notificationIds: req.body.notificationId }
    };

    const db = require("../../config/db").getDatabase();
    const adminCollection = db.collection(constants.database.collections.admins);
    const [updateError] = await execSilentCallback(adminCollection.updateOne, bindTo(adminCollection))(query, update);
    updateError.ifError("Failed to add notificationId to admin's profile").thenStopExecution();

    res.is.ok({ message: "Notification id added to admin account" });
}

function sendNewSignupNotificationToAdmin(name, phoneNumber, accountType) {
    const subject = `New ${accountType} signup`;
    const message = `${name} (${phoneNumber}) just registered as ${accountType}`;
    sendNotificationToAdmin(subject, message);
}

function sendTransactionNotificationToAdmin(txType, amount, descr, accountType) {
    const subject = `Transaction alert: ${accountType} ${txType}`;
    const message = `New transaction just completed for ${accountType}. Amount: ${amount}, Type: ${txType}`;
    sendNotificationToAdmin(subject, message, true);
}

function sendNotificationToAdmin(subject, message, ignoreSMS) {
    sendNotificationToAdminAsync(subject, message).catch(utils.logError);
}

async function sendNotificationToAdminAsync(subject, message, ignoreSMS) {
    // get all admin notification ids and phone numbers
    const db = require("../../config/db").getDatabase();
    const adminCollection = db.collection(constants.database.collections.admins);
    const findOptions = { projection: { phoneNumber: true, notificationIds: true } };
    const [error, cursor] = await execSilentCallback(adminCollection.find, bindTo(adminCollection))({}, findOptions);
    error.ifError("Error fetching admin to notify").thenStopExecution();

    const admins = await cursor.toArray();
    if (!ignoreSMS) {
        const smsRecipients = admins.filter(admin => admin.phoneNumber).map(admin => `234${admin.phoneNumber.substr(1)}`).join(",");
        (smsRecipients, message).catch(utils.logError);
    }

    for (const admin of admins) {
        const account = {
            id: admin._id,
            collection: constants.database.collections.admins
        };
        sendNotificationToApp(admin.notificationIds, subject, message, account).catch(utils.logError);
    }
}

module.exports = {
    registerFCMID,
    sendNewSignupNotificationToAdmin,
    sendTransactionNotificationToAdmin,
    sendNotificationToAdmin
};