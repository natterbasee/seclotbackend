const { bindTo, execSilentCallback, silentPromise } = require("silent-promise");
const notifications = require("../general/notifications");
const messaging = require("../general/messaging");
const utils = require("../../utils");
const constants = require("../../utils/constants");

async function sendMessage(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["message"], res)) {
        return;
    }

    const sendSMS = !req.body.ignoreSMS;
    const sendInApp = !req.body.ignoreApp;
    if (!sendSMS && !sendInApp) {
        res.is.badRequest(400, "Must send message either via SMS or to App");
        return;
    }

    if (req.body.location) {
        if (!req.body.location.latitude || !req.body.location.longitude || !req.body.location.radius) {
            res.is.badRequest(400, "location should be an object containing latitude, longitude and radius");
            return;
        }
    }

    const query = {};
    if (req.body.organizationId) {
        const userIds = await getAllUsersInOrganization(req.body.organizationId);
        if (userIds.length === 0) {
            res.is.ok({ recipientsCount: 0 });
            return;
        }

        query._id = { $in: userIds };
    }
    if (req.body.location) {
        query.location = {
            $nearSphere: {
                $geometry: { type: "Point", coordinates: [req.body.location.longitude, req.body.location.latitude] },
                $maxDistance: req.body.location.radius
            }
        }
    }

    const db = require("../../config/db").getDatabase();
    const usersCollection = db.collection(constants.database.collections.users);
    const options = { projection: { notificationIds: true } };
    const [findError, cursor] = await execSilentCallback(usersCollection.find, bindTo(usersCollection))(query, options);
    findError.ifError("failed to get beneficiaries info from database").thenStopExecution();

    const users = await cursor.toArray();
    const message = req.body.message;
    const subject = req.body.subject || `New message from Seclot Admin`;

    for (const user of users) {
        if (sendInApp === true) {
            const userAccount = {
                id: user._id,
                collection: constants.database.collections.users
            };
            await silentPromise(notifications.sendNotificationToApp(user.notificationIds, subject, message, userAccount))
        }
        if (sendSMS === true) {
            await silentPromise(messaging.sendSMS(user._id, message, req.body.smsSenderName));
        }
    }

    res.is.ok({ recipientsCount: users.length });
}

async function getAllUsersInOrganization(organizationId) {
    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    const query = { _id: organizationId };
    const projection = { projection: { beneficiaries: true, disabledBeneficiaries: true } };

    const [updateError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, projection);
    updateError.ifError("Failed to get beneficiaries from database").thenStopExecution();

    const beneficiaryIds = [];
    if (org.beneficiaries) {
        beneficiaryIds.push(...org.beneficiaries);
    }
    if (org.disabledBeneficiaries) {
        beneficiaryIds.push(...org.disabledBeneficiaries);
    }

    return beneficiaryIds;
}

module.exports = {
    sendMessage
};