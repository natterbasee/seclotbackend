const { bindTo, execSilentCallback } = require("silent-promise");
const utils = require("../../utils");
const constants = require("../../utils/constants");

async function registerFCMID(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["notificationId"], res)) {
        return;
    }
    if (typeof req.body.notificationId !== "string") {
        res.is.badRequest(400, "Invalid notificationId");
        return;
    }

    const query = { _id: req.user.id };
    const update = {
        $addToSet: { notificationIds: req.body.notificationId }
    };

    const db = require("../../config/db").getDatabase();
    const usersCollection = db.collection(constants.database.collections.users);
    const [updateError] = await execSilentCallback(usersCollection.updateOne, bindTo(usersCollection))(query, update);
    updateError.ifError("Failed to add notificationId to user's profile").thenStopExecution();

    res.is.ok({ message: "Notification id added to user account" })
}

module.exports = {
    registerFCMID
};