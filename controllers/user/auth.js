const bcrypt = require('bcrypt');
const randomNumber = require('random-number-csprng');
const redis = require('redis').createClient();
const {bindTo, execSilentCallback} = require('silent-promise');
const utils = require('../../utils');
const constants = require('../../utils/constants');
const {sendSMS} = require('../general/messaging');
const {generateToken, generateLimitedPrivilegesToken} = require('./token');
const {sendNewSignupNotificationToAdmin} = require('../admin/notification');
const {getNextId} = require('../general/auth');

async function login(req, res) {		
	if (utils.parametersAreIncomplete(req.body, ['phoneNumber', 'pin'], res)) {
		return;
	}

	const db = require('../../config/db').getDatabase();

	// get user record from db with phone number
	const usersCollection = db.collection(constants.database.collections.users);
	const [findUserError, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)({_id: req.body.phoneNumber});
	findUserError
		.ifError('Failed to get user info from database')
		.thenStopExecution();

	if (!user) {
		res.is.badRequest(
			403,
			'No user account for this phone number. Please use the register option'
		);
	} else if (!user.pinHash) {
		res.is.badRequest(
			403,
			'This account does not have a PIN. Please use the register option'
		);
	} else if (bcrypt.compareSync(req.body.pin, user.pinHash)) {
		res.is.ok({
			token: generateToken(user._id),
			profile: utils.getFormattedProfile(user, req)
		});
	} else if (user.isSuspended) {
		res.is.badRequest(403, 'This account has been suspended by the Admin');
	} else {
		res.is.badRequest(401, 'PIN is incorrect');
	}
}

async function requestOTP(req, res) {
	if (utils.parametersAreIncomplete(req.query, ['phoneNumber'], res)) {
		return;
	}

	if (req.query.phoneNumber.length !== 11) {
		res.is.badRequest(400, 'Phone number must be 11 digits');
		return;
	}

	if (req.query.phoneNumber.split().some(digit => isNaN(digit))) {
		res.is.badRequest(
			400,
			'Phone number cannot contain non-numerical characters'
		);
		return;
	}

	const codeMin = 1000,
		codeMax = 9999;
	let otpCode;

	// generate random code
	while (!otpCode) {
		const [generateCodeError, code] = await execSilentCallback(
			randomNumber,
			bindTo(randomNumber)
		)(codeMin, codeMax);
		generateCodeError.ifError('error generating otp code').thenStopExecution();

		if (code >= codeMin && code <= codeMax) {
			otpCode = code;
		}
	}

	// save against phone number in redis hash
	const phoneNumber = req.query.phoneNumber;
	const [saveOtpError] = await execSilentCallback(redis.hset, bindTo(redis))(
		constants.otpRedisKey,
		phoneNumber,
		otpCode.toString()
	);
	saveOtpError.ifError('error saving otp code').thenStopExecution();

	await sendSMS(
		phoneNumber,
		`Login to Seclot with the 4-digit number ${otpCode}`
	);

	res.is.ok({
		otpRequired: true,
		message: 'OTP has been sent to provided phone number'
	});
}

async function forgotPassowrd(req, res) {
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const [findUserError, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)({_id: req.query.phoneNumber});
	findUserError
		.ifError('Failed to get user info from database')
		.thenStopExecution();

	if (user) {
		await requestOTP(req, res);
	} else {
		res.is.badRequest(400, 'You do not have an account on seclot!');
		return;
	}
}

async function resendOTP(req, res) {
	if (utils.parametersAreIncomplete(req.query, ['phoneNumber'], res)) {
		return;
	}

	if (req.query.phoneNumber.length !== 11) {
		res.is.badRequest(400, 'Phone number must be 11 digits');
		return;
	}

	if (req.query.phoneNumber.split().some(digit => isNaN(digit))) {
		res.is.badRequest(
			400,
			'Phone number cannot contain non-numerical characters'
		);
		return;
	}

	// get saved otp for this number from redis
	const [error, savedOtp] = await execSilentCallback(redis.hget, bindTo(redis))(
		constants.otpRedisKey,
		req.query.phoneNumber
	);
	error.ifError('error verifying otp').thenStopExecution();

	if (savedOtp) {
		await sendSMS(
			req.query.phoneNumber,
			`Login to Seclot with the 4-digit number ${savedOtp}`
		);
		res.is.ok({
			otpRequired: true,
			message: 'OTP has been sent to provided phone number'
		});
		return;
	}

	// check if user exists and

	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const [findUserError, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)({_id: req.query.phoneNumber});
	findUserError
		.ifError('Failed to get user info from database')
		.thenStopExecution();

	if (user) {
		await requestOTP(req, res);
	} else {
		res.is.badRequest(400, 'You do not have an account on seclot!');
		return;
	}
}

async function verifyOTP(req, res) {
	if (utils.parametersAreIncomplete(req.query, ['phoneNumber', 'otp'], res)) {
		return;
	}

	// get saved otp for this number from redis and compare
	const [error, savedOtp] = await execSilentCallback(redis.hget, bindTo(redis))(
		constants.otpRedisKey,
		req.query.phoneNumber
	);
	error.ifError('error verifying otp').thenStopExecution();

	if (req.query.otp !== savedOtp) {
		res.is.badRequest(400, 'Invalid OTP');
		return;
	}

	// pin verified, remove from redis
	await execSilentCallback(redis.hdel, bindTo(redis))(
		constants.otpRedisKey,
		req.query.phoneNumber
	);

	// get user record from db with phone number
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const [findUserError, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)({_id: req.query.phoneNumber});
	findUserError
		.ifError('Failed to get user info from database')
		.thenStopExecution();

	// this endpoint only returns limited user privileges token, see responses below
	const authCode = generateLimitedPrivilegesToken(req.query.phoneNumber);

	if (user && user.pinHash) {
		// user account seems fully set, user should have been able to log in with PIN, unless he forgot his PIN, needs to reset PIN
		res.is.ok({authCode, newPinRequired: true});
	} else {
		// either user account does not exist, or if it does, it's not fully set because it was added by organization, ask user to register
		res.is.ok({authCode, newUser: true});
	}
}

async function register(req, res) {
	if (
		utils.parametersAreIncomplete(
			req.body,
			['firstName', 'lastName', 'email', 'pin', 'location'],
			res
		)
	) {
		return;
	}

	if (typeof req.body.pin === 'number') {
		req.body.pin = req.body.pin.toString();
	}
	if (
		typeof req.body.pin !== 'string' ||
    req.body.pin.length < 4 ||
    req.body.pin.split().some(digit => isNaN(digit))
	) {
		res.is.badRequest(
			400,
			'Invalid pin. PIN must be at least 4 digits and must be only numbers'
		);
		return;
	}

	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);

	// check if this number is already in use by a fully registered user
	const registeredQuery = {_id: req.user.id, pinHash: {$exists: true}};
	const [findUserError, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)(registeredQuery);
	findUserError
		.ifError('Failed to get user info from database')
		.thenStopExecution();
	if (user) {
		res.is.badRequest(400, 'This phone number is tied to another user account');
		return;
	}

	const lat = req.body.location.latitude,
		long = req.body.location.longitude;
	const location = {
		type: 'Point',
		coordinates: [long, lat]
	};
	const query = {_id: req.user.id};
	let userInfo = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		pinHash: bcrypt.hashSync(req.body.pin.trim(), 10),
		location,
		lastLocationUpdate: Date.now(),
		accountCreationDate: Date.now()
	};
	const update = {$set: userInfo};
	const options = {returnOriginal: false};

	// try to update user profile first assuming user was already added as beneficiary by organization
	const [updateError, updateResult] = await execSilentCallback(
		usersCollection.findOneAndUpdate,
		bindTo(usersCollection)
	)(query, update, options);
	updateError
		.ifError('Failed to create or update user account')
		.thenStopExecution();

	if (updateResult.value) {
		userInfo = updateResult.value;
	} else {
		// new user, create account
		userInfo._id = req.user.id;
		userInfo.seclotId = await getNextId();

		const [error] = await execSilentCallback(
			usersCollection.insertOne,
			bindTo(usersCollection)
		)(userInfo);
		error.ifError('Failed to create user account').thenStopExecution();
	}

	sendNewSignupNotificationToAdmin(
		`${userInfo.firstName} ${userInfo.lastName}`,
		userInfo._id,
		'user'
	);
	res.is.ok({
		token: generateToken(userInfo._id),
		profile: utils.getFormattedProfile(userInfo, req)
	});
}

async function setPIN(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['pin'], res)) {
		return;
	}

	if (typeof req.body.pin === 'number') {
		req.body.pin = req.body.pin.toString();
	}
	if (
		typeof req.body.pin !== 'string' ||
    req.body.pin.length < 4 ||
    req.body.pin.split().some(digit => isNaN(digit))
	) {
		res.is.badRequest(
			400,
			'Invalid pin. PIN must be at least 4 digits and must be only numbers'
		);
		return;
	}

	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);

	// update pinHash if user account exists and has already set PIN before
	const query = {
		$and: [{_id: req.user.id}, {pinHash: {$exists: true}}]
	};
	const updateProfile = {
		$set: {pinHash: bcrypt.hashSync(req.body.pin.trim(), 10)}
	};
	const options = {returnOriginal: false};
	const [error, updateResult] = await execSilentCallback(
		usersCollection.findOneAndUpdate,
		bindTo(usersCollection)
	)(query, updateProfile, options);
	error.ifError('PIN update error').thenStopExecution();
	const userInfo = updateResult.value;
	if (!userInfo) {
		res.is.badRequest(
			400,
			'This phone number is not tied to a user account. Do you mean to create a new account?'
		);
	} else {
		res.is.ok({
			token: generateToken(userInfo._id),
			profile: utils.getFormattedProfile(userInfo, req)
		});
	}
}

module.exports = {
	login,
	requestOTP,
	forgotPassowrd,
	resendOTP,
	verifyOTP,
	register,
	setPIN
};
