const bcrypt = require('bcrypt');
const multer = require('multer');
const ObjectID = require('mongodb').ObjectID;
const {join, extname} = require('path');
const {unlinkSync} = require('fs');
const {bindTo, execSilentCallback} = require('silent-promise');
const utils = require('../../utils');
const constants = require('../../utils/constants');
const logger = require('../../utils/logger');
const {getAccountIdWithSeclotId} = require('../general/auth');
const {creditAccount} = require('../general/transactions');
const settings = require('../general/settings');

async function getProfile(req, res) {
	const query = {_id: req.user.id};
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const [error, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)(query);
	error
		.ifError('Failed to update user\'s subscription plan')
		.thenStopExecution();
	res.is.ok(user);
}
/*
The following are optional
*Referral ID*
*Health sector*
*Security sector*
(can't recall what those last fields mean by the way)
 */
async function updateProfile(req, res) {
	const updateFields = [
		'firstName',
		'lastName',
		'pin',
		'email',
		'address',
		'location',
		'referralId'
	];
	if (utils.hasNoneOfTheRequiredParameters(req.body, updateFields, res)) {
		return;
	}

	let pinHash;
	if (req.body.pin) {
		if (typeof req.body.pin === 'number') {
			req.body.pin = req.body.pin.toString();
		}
		if (
			typeof req.body.pin !== 'string' ||
      req.body.pin.length < 4 ||
      req.body.pin.split().some(digit => isNaN(digit))
		) {
			res.is.badRequest(
				400,
				'Invalid pin. PIN must be at least 4 digits and must be only numbers'
			);
			return;
		}

		pinHash = bcrypt.hashSync(req.body.pin.trim(), 10);
		delete req.body.pin;
	}

	// check if referralId was already set, then remove from update list
	if (req.user.referralId) {
		delete req.body.referralId;
	}

	if (req.body.referralId) {
		req.body.referralId = +req.body.referralId;

		// check if trying to refer self
		if (req.body.referralId === req.user.seclotId) {
			res.is.badRequest(400, 'Invalid referralId. Cannot refer yourself!');
			return;
		}

		// first time setting referralId, check if referral is valid
		const referralAccount = await getAccountIdWithSeclotId(req.body.referralId);
		if (!referralAccount) {
			res.is.badRequest(400, 'referralId is not valid');
			return;
		}

		// give referral bonus to referral
		const txId = new ObjectID().toHexString();
		const shortDescription = 'Referral Earning',
			longDescription = `From referred user (${req.user.name})`;
		const referralBonus = await settings.getReferralAmount();
		await creditAccount(
			referralAccount.type,
			referralAccount.id,
			txId,
			Date.now(),
			referralBonus,
			shortDescription,
			longDescription
		);
	}

	const update = {};
	if (pinHash) {
		update.pinHash = pinHash;
	}
	for (const field of updateFields) {
		if (!req.body[field]) {
			continue;
		}

		if (field === 'location') {
			const lat = req.body[field].latitude,
				long = req.body[field].longitude;
			if (lat && long) {
				update[field] = {
					type: 'Point',
					coordinates: [long, lat]
				};
				update.lastLocationUpdate = Date.now();
			}
		} else if (field !== 'pin') {
			update[field] = req.body[field];
		}
	}

	const updateProfile = {$set: update};
	const options = {returnOriginal: false};

	const query = {_id: req.user.id};
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const [updateError, updateResult] = await execSilentCallback(
		usersCollection.findOneAndUpdate,
		bindTo(usersCollection)
	)(query, updateProfile, options);
	updateError
		.ifError('Failed to update user profile on database')
		.thenStopExecution();

	res.is.ok(utils.getFormattedProfile(updateResult.value, req));
}

async function updateProfilePicture(req, res) {
	const [uploadError] = await execSilentCallback(
		getMulterInstance().single('image'),
		req,
		res
	);
	const error = uploadError.getError();
	if (error) {
		switch (error.code) {
		case 'LIMIT_UNEXPECTED_FILE':
			res.is.badRequest(400, `Expected 'image' field, got '${error.field}'`);
			break;

		default:
			logger.error(
				{detail: error, path: req.path, method: req.method},
				'Profile picture upload error'
			);
			res.is.serverError('Error processing file');
			break;
		}
		return;
	}

	if (!req.file) {
		return res.is.badRequest(400, 'Missing file parameter: image');
	}

	const updateProfile = {$set: {picture: req.file.filename}};
	const options = {returnOriginal: false};

	const query = {_id: req.user.id};
	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);
	const [updateError, updateResult] = await execSilentCallback(
		usersCollection.findOneAndUpdate,
		bindTo(usersCollection)
	)(query, updateProfile, options);
	updateError
		.ifError('Failed to update user profile on database')
		.thenStopExecution();

	// delete previous image file
	const previousProfilePictureFileName = req.user.picture;
	if (previousProfilePictureFileName) {
		const path = join(process.env.UPLOAD_PATH, previousProfilePictureFileName);
		unlinkSync(path);
	}

	res.is.ok(utils.getFormattedProfile(updateResult.value, req));
}

function getMulterInstance() {
	const profileImageStorage = multer.diskStorage({
		destination: (req, file, cb) => {
			cb(undefined, process.env.UPLOAD_PATH);
		},
		filename: (req, file, cb) => {
			cb(undefined, `profile_image_${Date.now()}${extname(file.originalname)}`);
		}
	});
	const imageFilter = (req, file, cb) => {
		cb(undefined, file.mimetype.startsWith('image'));
	};

	return multer({
		storage: profileImageStorage,
		fileFilter: imageFilter,
		limits: {fieldSize: 2}
	});
}

module.exports = {
	getProfile,
	updateProfile,
	updateProfilePicture
};
