const {bindTo, execSilentCallback} = require('silent-promise');
const ObjectID = require('mongodb').ObjectID;
const utils = require('../../utils');
const constants = require('../../utils/constants');
const {sendSMS} = require('../general/messaging');
const {sendNotificationToApp} = require('../general/notifications');

async function getICEs(req, res) {
	let query = {
		'addedBy.accountId': req.user.id,
		'addedBy.accountType': 'user'
	};
	const options = {projection: {addedBy: false}};

	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	let findError, cursor;

	[findError, cursor] = await execSilentCallback(
		iceCollection.find,
		bindTo(iceCollection)
	)(query, options);
	findError
		.ifError('failed to get user ICEs from database')
		.thenStopExecution();

	let userICEs = await cursor.toArray();
	userICEs = userICEs.map(ice => {
		ice.id = ice._id;
		delete ice._id;
		return ice;
	});

	// get ICEs assigned to user by organization
	const userICEsCollection = db.collection(
		constants.database.collections.user_ices
	);
	query = {userId: req.user.id};
	options.projection = {status: true, iceID: true, assignDate: true};
	[findError, cursor] = await execSilentCallback(
		userICEsCollection.find,
		bindTo(userICEsCollection)
	)(query, options);
	findError
		.ifError('failed to get organization-assigned ICEs from database')
		.thenStopExecution();

	const orgCollection = db.collection(
		constants.database.collections.organizations
	);
	const organizationICEs = [];
	while (await cursor.hasNext()) {
		const iceLink = await cursor.next();

		// get ice info using ice id
		query = {_id: iceLink.iceID};
		options.projection = {name: true, addedBy: true};
		const [getICEError, ice] = await execSilentCallback(
			iceCollection.findOne,
			bindTo(iceCollection)
		)(query, options);
		getICEError.ifError('Error reading ICE info').thenStopExecution();

		if (!ice) {
			continue;
		}

		// get organization name
		query = {_id: ice.addedBy.accountId};
		options.projection = {name: true};
		const [getOrgError, org] = await execSilentCallback(
			orgCollection.findOne,
			bindTo(orgCollection)
		)(query, options);
		getOrgError
			.ifError('Error reading super organization info')
			.thenStopExecution();

		ice.organization = org.name;
		ice.paused = iceLink.status === 'paused';
		ice.assignDate = iceLink.assignDate;
		ice.id = ice._id;
		ice.phoneNumber = ice.phoneNumber;
		delete ice._id;
		delete ice.addedBy;

		organizationICEs.push(ice);
	}

	res.is.ok({userICEs, organizationICEs});
}

async function addICE(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['name', 'phoneNumber'], res)) {
		return;
	}

	if (req.body.phoneNumber.length !== 11) {
		res.is.badRequest(400, 'Phone number should be 11 digits');
		return;
	}
	if (req.body.phoneNumber.split().some(digit => isNaN(digit))) {
		res.is.badRequest(400, 'Phone number is invalid');
		return;
	}

	// if (req.user.accountStatus !== 'enabled' || !req.user.plan) {
	// 	res.is.badRequest(
	// 		400,
	// 		'Cannot add an ICE without an active subscription plan'
	// 	);
	// 	return;
	// }

	const userId = req.user.id;
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);

	// count number of ICEs added by user, cannot be more than 10
	const query = {
		'addedBy.accountId': req.user.id,
		'addedBy.accountType': 'user'
	};
	const [error, userICECount] = await execSilentCallback(
		iceCollection.countDocuments,
		bindTo(iceCollection)
	)(query);
	error.ifError('Error getting ICE count for user').thenStopExecution();

	if (userICECount >= 10) {
		res.is.badRequest(
			400,
			'User can only add 10 ICEs. Delete previous ICEs to add more'
		);
		return;
	}

	// check for any other ICE added by this organization with the same name or phone number
	const duplicateQuery = {
		phoneNumber: req.body.phoneNumber,
		// name: req.body.name,
		'addedBy.accountType': 'user'
	};
	const options = {projection: {name: true, phoneNumber: true}};
	const [findError, ice] = await execSilentCallback(
		iceCollection.findOne,
		bindTo(iceCollection)
	)(duplicateQuery, options);
	findError
		.ifError('Error checking new ICE info for duplicated info')
		.thenStopExecution();

	if (ice) {
		const phoneExists = ice.phoneNumber === req.body.phoneNumber;
		const nameExists = ice.name === req.body.name;

		let errorMessage = 'Another ICE exists with the same ';
		if (phoneExists && nameExists) {
			errorMessage += 'name and phone number';
		} else if (phoneExists) {
			errorMessage += 'phone number';
		} else if (nameExists) {
			errorMessage += 'name';
		}
		res.is.badRequest(400, errorMessage);
		return;
	}

	const newICE = {
		_id: new ObjectID().toHexString(),
		name: req.body.name,
		phoneNumber: req.body.phoneNumber,
		addedBy: {
			accountId: userId,
			accountType: 'user'
		},
		paused: false,
		dateAdded: Date.now()
	};
	const [saveError] = await execSilentCallback(
		iceCollection.insertOne,
		bindTo(iceCollection)
	)(newICE);
	saveError.ifError('Failed to add ICE to user\'s account').thenStopExecution();
	const notificationMessage = `You have been added as an ICE by ${req.user.id}`;

	sendSMS(
		newICE.phoneNumber,
		`You have been added as an ICE by ${req.user.id} on seclot. 
	Download the app here https://play.google.com/store/apps/details?id=com.seclotmobile`
	);

	const userCollection = db.collection(constants.database.collections.users);
	const [getUserError, usersCursor] = await execSilentCallback(
		userCollection.find,
		bindTo(userCollection)
	)({
		_id: req.body.phoneNumber
	});

	getUserError
		.ifError('Failed to get detailed user ices info from database')
		.thenStopExecution();

	const users = await usersCursor.toArray();

	const Ids = users.map(user => {
		return {
			notificationIds: user.notificationIds
				? user.notificationIds.toString()
				: '',
			id: user._id
		};
	});

	for (const id of Ids) {
		sendNotificationToApp(
			[id.notificationIds],
			'You have been added as an Ice',
			notificationMessage,
			{
				id: id.id,
				collection: constants.database.collections.users
			}
		);
	}

	await getICEs(req, res);
}

async function updateICE(req, res) {
	if (
		utils.hasNoneOfTheRequiredParameters(
			req.body,
			['name', 'phoneNumber', 'pause'],
			res
		)
	) {
		return;
	}
	if (req.body.phoneNumber) {
		if (req.body.phoneNumber.length !== 11) {
			res.is.badRequest(400, 'Phone number should be 11 digits');
			return;
		}
		if (req.body.phoneNumber.split().some(digit => isNaN(digit))) {
			res.is.badRequest(400, 'Phone number is invalid');
			return;
		}
	}
	if (req.body.pause && req.body.pause !== true && req.body.pause !== false) {
		res.is.badRequest(400, 'pause should either be true or false');
		return;
	}

	// if (req.user.accountStatus !== 'enabled' || !req.user.plan) {
	// 	res.is.badRequest(
	// 		400,
	// 		'Cannot update an ICE without an active subscription plan'
	// 	);
	// 	return;
	// }

	if (await tryUpdateOrgAssignedICEStatus(req, res)) {
		await getICEs(req, res);
		return;
	}

	const userId = req.user.id;
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);

	const update = {};
	if (req.body.pause !== '') {
		update.paused = req.body.pause;
	}

	// check for any other ICE added by this user with the same name or phone number that is NOT this ICE to be updated
	if (req.body.name || req.body.phoneNumber) {
		const duplicateQuery = {
			_id: {$ne: req.params.iceId},
			'addedBy.accountId': userId,
			'addedBy.accountType': 'user',
			$or: []
		};

		if (req.body.name) {
			update.name = req.body.name;
			duplicateQuery['$or'].push({name: req.body.name});
		}
		if (req.body.phoneNumber) {
			update.phoneNumber = req.body.phoneNumber;
			duplicateQuery['$or'].push({phoneNumber: req.body.phoneNumber});
		}

		const options = {projection: {name: true, phoneNumber: true}};
		const [findError, ice] = await execSilentCallback(
			iceCollection.findOne,
			bindTo(iceCollection)
		)(duplicateQuery, options);
		findError
			.ifError('Error checking update info for duplicated info')
			.thenStopExecution();

		if (ice) {
			const phoneExists = ice.phoneNumber === req.body.phoneNumber;
			const nameExists = ice.name === req.body.name;

			let errorMessage = 'Another ICE exists with the same ';
			if (phoneExists && nameExists) {
				errorMessage += 'name and phone number';
			} else if (phoneExists) {
				errorMessage += 'phone number';
			} else if (nameExists) {
				errorMessage += 'name';
			}

			res.is.badRequest(400, errorMessage);
			return;
		}
	}

	const query = {_id: req.params.iceId};
	const [saveError, opsResult] = await execSilentCallback(
		iceCollection.updateOne,
		bindTo(iceCollection)
	)(query, {$set: update});
	saveError.ifError('Failed to update ICE information').thenStopExecution();

	if (opsResult.matchedCount === 0) {
		res.is.badRequest(400, 'Invalid ICE ID');
		return;
	}

	await getICEs(req, res);
}

async function pauseIce(req, res) {
	if (utils.parametersAreIncomplete(req.params, ['iceId'], res)) {
		return;
	}
	const query = {_id: req.params.iceId};
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	const [saveError, opsResult] = await execSilentCallback(
		iceCollection.updateOne,
		bindTo(iceCollection)
	)(query, {$set: {paused: true}});
	saveError.ifError('Failed to update ICE information').thenStopExecution();

	if (opsResult.matchedCount === 0) {
		res.is.badRequest(400, 'Invalid ICE ID');
		return;
	}
	res.is.ok({message: 'Ice paused successfully from alerts'});
}

async function unPauseIce(req, res) {
	if (utils.parametersAreIncomplete(req.params, ['iceId'], res)) {
		return;
	}
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	const query = {_id: req.params.iceId};
	const [saveError, opsResult] = await execSilentCallback(
		iceCollection.updateOne,
		bindTo(iceCollection)
	)(query, {$set: {paused: false}});
	saveError.ifError('Failed to update ICE information').thenStopExecution();

	if (opsResult.matchedCount === 0) {
		res.is.badRequest(400, 'Invalid ICE ID');
		return;
	}
	res.is.ok({message: 'Ice unpaused successfully from alerts'});
}

async function tryUpdateOrgAssignedICEStatus(req, res) {
	const userId = req.user.id;
	const iceID = req.params.iceId;

	const db = require('../../config/db').getDatabase();
	const userICEsCollection = db.collection(
		constants.database.collections.user_ices
	);

	// check if this ICE was assigned by an organization to this user
	const query = {iceID, userId};
	const options = {projection: {status: true}};
	const [getICEError, ice] = await execSilentCallback(
		userICEsCollection.findOne,
		bindTo(userICEsCollection)
	)(query, options);
	getICEError.ifError('Error checking if ICE exists').thenStopExecution();

	if (!ice) {
		return false;
	} else if (req.body.name || req.body.phoneNumber) {
		res.is.badRequest(
			400,
			'ICE was assigned by an organization. User cannot update name or phone number'
		);
		return true;
	}

	// update ice status
	const newStatus = req.body.pause === true ? 'paused' : 'active';
	const update = {
		$set: {status: newStatus}
	};
	const [updateError] = await execSilentCallback(
		userICEsCollection.updateOne,
		bindTo(userICEsCollection)
	)(query, update);
	updateError
		.ifError('Error pausing/unpausing ICE assigned by organization')
		.thenStopExecution();

	return true;
}

async function removeICE(req, res) {
	const db = require('../../config/db').getDatabase();
	let deleteError;

	let query = {
		_id: req.params.iceId,
		'addedBy.accountId': req.user.id,
		'addedBy.accountType': 'user'
	};

	const iceCollection = db.collection(constants.database.collections.ices);
	const [findError, ice] = await execSilentCallback(
		iceCollection.findOne,
		bindTo(iceCollection)
	)(query);

	sendSMS(
		ice.phoneNumber,
		`You have been removed as an ICE by ${req.user.firstName} ${req.user.lastName}, whose mobile number is ${req.user.id} on seclot. This is the location this event took place https://www.google.com/maps/search/?api=1&query=${req.user.location[0]},${req.user.location[1]}`
	);
	[deleteError, opsResult] = await execSilentCallback(
		iceCollection.deleteOne,
		bindTo(iceCollection)
	)(query);
	deleteError
		.ifError('Failed to remove ICE from user profile')
		.thenStopExecution();

	if (opsResult.deletedCount === 0) {
		res.is.badRequest(400, 'Invalid ICE ID or ICE belongs to organization');
		return;
	}

	await getICEs(req, res);
}

module.exports = {
	getICEs,
	unPauseIce,
	pauseIce,
	addICE,
	updateICE,
	removeICE
};
