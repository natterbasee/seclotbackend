const jwt  = require('jsonwebtoken');
const { join } = require("path");
const { readFileSync } = require("fs");
const privateKey = readFileSync(join(__dirname, '../../config/jwt-private.key'), 'utf8');
const publicKey = readFileSync(join(__dirname, '../../config/jwt-public.key'), 'utf8');

function generateToken(userId) {
    const payload = {
        userId: userId,
        privileges: "full"
    };
    const options = {
        expiresIn:  "7d",
        algorithm:  "RS256"
    };

    return jwt.sign(payload, privateKey, options);
}

function generateLimitedPrivilegesToken(userId) {
    const payload = {
        userId: userId,
        privileges: "limited"
    };
    const options = {
        expiresIn:  "7d",
        algorithm:  "RS256"
    };

    return jwt.sign(payload, privateKey, options);
}

function verifyToken(token) {
    try {
        const data = jwt.verify(token, publicKey);
        return { id: data.userId, privileges: data.privileges };
    }
    catch (_) {}
}

module.exports = {
    generateToken,
    generateLimitedPrivilegesToken,
    verifyToken
};