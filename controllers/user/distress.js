const {bindTo, execSilentCallback} = require('silent-promise');
const ObjectID = require('mongodb').ObjectID;
const logger = require('../../utils/logger');
const constants = require('../../utils/constants');
const {sendNotificationToApp} = require('../general/notifications');
const {sendSMS} = require('../general/messaging');
const {sendNotificationToAdmin} = require('../admin/notification');

async function distressCallsHistory(req, res) {
	const db = require('../../config/db').getDatabase();
	const distressCollection = db.collection(
		constants.database.collections.distressCalls
	);
	const query = {userId: req.user.id};

	const [error, cursor] = await execSilentCallback(
		distressCollection.find,
		bindTo(distressCollection)
	)(query);
	error
		.ifError('Error getting distress call history for user')
		.thenStopExecution();

	let history = await cursor.toArray();
	history = history.map(call => {
		delete call._id;
		delete call.userId;
		call.organizationsNotified = call.organizationsNotified.length;
		call.icesNotified = call.icesNotified.length;
		call.location = call.location || {};
		return call;
	});

	res.is.ok(history);
}

async function issueDistressCall(req, res) {
	if (req.user.accountStatus !== 'enabled' || !req.user.plan) {
		res.is.badRequest(
			400,
			'You are not on paid plan. You cannot issue distress call.'
		);
		return;
	}

	if (req.body.location) {
		if (
			!req.body.location.latitude ||
      !req.body.location.longitude ||
      !req.body.location.radius
		) {
			res.is.badRequest(
				400,
				'location should be an object containing latitude, longitude and radius'
			);
			return;
		}
	} else {
		return res.is.badRequest(
			400,
			'Please send location object containing latitude, longitude and radius'
		);
	}

	const db = require('../../config/db').getDatabase();

	// get ICEs tied to user that are not paused
	// let query = {userId: req.user.id, status: 'active', pause: false};
	// let options = {projection: {iceID: true}};

	let query = {
		addedBy: {accountId: req.user.id, accountType: 'user'}
	};
	const userICEsCollection = db.collection(constants.database.collections.ices);
	const [getUserICELinksError, userICELinksCursor] = await execSilentCallback(
		userICEsCollection.find,
		bindTo(userICEsCollection)
	)(query);
	getUserICELinksError
		.ifError('Failed to get user\'s ices list from database')
		.thenStopExecution();

	const userICELinks = await userICELinksCursor.toArray();
	const userIceIDs = userICELinks.map(ice => ice._id);
	const userPhoneNumber = userICELinks.map(ice => ice.phoneNumber);

	query = {
		_id: {$in: userPhoneNumber}
	};
	const userCollection = db.collection(constants.database.collections.users);
	const [getUserError, usersCursor] = await execSilentCallback(
		userCollection.find,
		bindTo(userCollection)
	)(query);

	getUserError
		.ifError('Failed to get detailed user ices info from database')
		.thenStopExecution();

	const users = await usersCursor.toArray();

	const Ids = users.map(user => {
		return {
			notificationIds: user.notificationIds
				? user.notificationIds.toString()
				: '',
			id: user._id
		};
	});

	// if user account is enabled, get user added ICEs to notify, along with organization assigned ICEs
	if (req.user.accountStatus === 'enabled') {
		query = {
			$or: [
				{_id: {$in: userIceIDs}},
				{
					'addedBy.accountId': req.user.id,
					'addedBy.accountType': 'user',
					paused: false
				}
			]
		};
	} else {
		// use ids to get phone numbers of ices and organizations user is tied to
		query = {
			$and: [
				{
					_id: {
						$in: userIceIDs
					}
				},
				{
					paused: false
				}
			]
		};
	}

	options = {projection: {addedBy: true, phoneNumber: true}};
	const iceCollection = db.collection(constants.database.collections.ices);
	const [getUserICEsError, userICEsCursor] = await execSilentCallback(
		iceCollection.find,
		bindTo(iceCollection)
	)(query, options);

	getUserICEsError
		.ifError('Failed to get detailed user ices info from database')
		.thenStopExecution();

	const userICEs = await userICEsCursor.toArray();

	const icesForSms = new Set();
	const organizationIDs = new Set();
	for (const ice of userICEs) {
		if (!ice.paused) {
			icesForSms.add(ice.phoneNumber);
		}
		if (!ice.paused && ice.addedBy.accountType === 'organization') {
			organizationIDs.add(ice.addedBy.accountId);
		}
	}

	// use organization ids to check if organization is disabled (owing) or if the organization disabled the user
	query = {
		_id: {$in: Array.from(organizationIDs)}
	};
	options = {
		projection: {
			disabledBeneficiaries: true,
			accountStatus: true,
			notificationIds: true
		}
	};
	const orgCollection = db.collection(
		constants.database.collections.organizations
	);
	const [getOrgsInfoError, orgsInfoCursor] = await execSilentCallback(
		orgCollection.find,
		bindTo(orgCollection)
	)(query, options);
	getOrgsInfoError
		.ifError(
			'failed to get info of organizations related to user from database'
		)
		.thenDo(logError);

	let organizationIdsToIgnore = new Set();
	const organizationsToContactNotificationIDs = {};
	if (orgsInfoCursor) {
		const orgsInfo = await orgsInfoCursor.toArray();
		for (const org of orgsInfo) {
			if (
				org.accountStatus !== 'enabled' ||
        org.disabledBeneficiaries.indexOf(req.user.id) >= 0
			) {
				organizationIdsToIgnore.add(org._id);
			} else {
				organizationsToContactNotificationIDs[org._id] = org.notificationIds;
			}
		}
	} else {
		organizationIdsToIgnore = organizationIDs;
	}

	// at the end of the day, these are the ices (phone numbers) and organizations (notificationIds) we will send notification to
	const notifyICEs = [],
		notifyICENumbers = [],
		notifyOrganizations = [];
	for (const ice of userICEs) {
		// notify organization?
		if (
			ice.addedBy.accountType === 'organization' &&
      organizationIDs.has(ice.addedBy.accountId)
		) {
			notifyOrganizations.push({
				id: ice.addedBy.accountId,
				notificationIds:
          organizationsToContactNotificationIDs[ice.addedBy.accountId]
			});
			// delete this id to prevent duplicate sending of notifications
			organizationIDs.delete(ice.addedBy.accountId);
		}

		// notify ICE?
		if (
			ice.addedBy.accountType === 'user' ||
      (ice.addedBy.accountType === 'organization' &&
        !organizationIdsToIgnore.has(ice.addedBy.accountId))
		) {
			notifyICEs.push(ice._id);
			notifyICENumbers.push(ice.phoneNumber);
		}
	}

	const notificationSubject = 'Distress Alert!';
	const notificationMessage = `${req.user.firstName} ${req.user.lastName} (${req.user.id}) is in distress. Please respond ASAP. This is the location https://www.google.com/maps/search/?api=1&query=<${req.user.location[0]}>,<${req.user.location[1]}>`;

	// let's notify, shall we?
	for (const org of notifyOrganizations) {
		sendNotificationToApp(
			org.notificationIds,
			notificationSubject,
			notificationMessage,
			{id: org.id, collection: constants.database.collections.organizations}
		).catch(logError);
	}

	for (const id of Ids) {
		sendNotificationToApp(
			[id.notificationIds],
			notificationSubject,
			notificationMessage,
			{
				id: id.id,
				collection: constants.database.collections.users
			}
		);
	}

	for (const phoneNumber of notifyICENumbers) {
		sendSMS(phoneNumber, notificationMessage).catch(logError);
	}

	// save a record of this distress call for later
	const distressCollection = db.collection(
		constants.database.collections.distressCalls
	);
	const distressInfo = {
		_id: new ObjectID().toHexString(),
		userId: req.user.id,
		icesNotified: notifyICEs,
		icePhoneNumbersNotified: notifyICENumbers,
		organizationsNotified: notifyOrganizations.map(org => org.id),
		status: 'pending',
		callDate: Date.now(),
		location: req.body.location
	};
	const [saveError] = await execSilentCallback(
		distressCollection.insertOne,
		bindTo(distressCollection)
	)(distressInfo);
	saveError
		.ifError('Error saving distress call record to history')
		.thenDo(logError);

	const adminMessage = `${req.user.firstName} ${req.user.lastName} (${req.user.id}) is in distress. ${notifyICEs.length} ICEs & ${notifyOrganizations.length} organizations notified`;
	sendNotificationToAdmin(notificationSubject, adminMessage);

	res.is.ok({
		icesNotified: notifyICEs.length,
		organizationsNotified: notifyOrganizations.length
	});
}

function logError(error, errorMessage) {
	const detail = error.stack ? error.stack : error;
	logger.error(
		{detail, path: 'issueDistressCall'},
		errorMessage || error.message
	);
}

module.exports = {
	distressCallsHistory,
	issueDistressCall
};
