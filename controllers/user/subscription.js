const {bindTo, execSilentCallback} = require('silent-promise');
const utils = require('../../utils');
const constants = require('../../utils/constants');
const settings = require('../../controllers/general/settings');

async function updateSubscriptionPlan(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['plan'], res)) {
		return;
	}
	if (typeof req.body.plan !== 'string') {
		res.is.badRequest(
			400,
			`Expected plan to be 'string', got ${typeof req.body.plan}`
		);
		return;
	}

	const plans = await settings.getSubscriptionPlans();
	let selectedPlan, selectedPlanName;
	for (const planName of Object.keys(plans)) {
		if (planName.toLowerCase() === req.body.plan.toLowerCase()) {
			selectedPlan = plans[planName];
			selectedPlanName = planName;
			break;
		}
	}
	if (!selectedPlan) {
		res.is.badRequest(400, 'Invalid plan selected');
		return;
	}

	const db = require('../../config/db').getDatabase();
	const usersCollection = db.collection(constants.database.collections.users);

	const query = {_id: req.user.id};
	const options = {projection: {plan: true, walletBalance: true}};
	const [error, user] = await execSilentCallback(
		usersCollection.findOne,
		bindTo(usersCollection)
	)(query, options);
	error
		.ifError('Failed to update user\'s subscription plan')
		.thenStopExecution();

	if (user.plan && user.plan.toLowerCase() === selectedPlanName.toLowerCase()) {
		res.is.badRequest(400, 'Already subscribed to this plan');
		return;
	}

	const nextBill = selectedPlan.amount;
	if (!user.walletBalance || user.walletBalance < nextBill) {
		res.is.badRequest(
			400,
			'You do not have enough funds to subscribe to this plan. Please fund your wallet before subscribing'
		);
		return;
	}

	// save plan to profile, set status as active and mark next billing date as now
	const orgUpdate = {
		plan: selectedPlanName,
		subscriptionStatus: 'active',
		nextBillDate: Date.now()
	};

	const [updateError] = await execSilentCallback(
		usersCollection.updateOne,
		bindTo(usersCollection)
	)(query, {$set: orgUpdate});
	updateError
		.ifError('Failed to update user\'s subscription plan')
		.thenStopExecution();

	return res.is.ok(orgUpdate);
}

module.exports = {
	updateSubscriptionPlan
};
