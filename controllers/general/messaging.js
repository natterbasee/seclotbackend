const rp = require('request-promise');
const twilio = require('twilio');
const accountSid = process.env.TWILIO_ACCOUNT_SID; // Your Account SID from www.twilio.com/console
const authToken = process.env.TWILIO_AUTH_TOKEN; // Your Auth Token from www.twilio.com/console

async function sendSMS(recipient, message, senderId) {
	const num = [];
	if (Array.isArray(recipient)) {
		for (let number of recipient) {
			if (number[0] === '0') {
				number = `+234${number.substr(1)}`;
			}
			num.push(number);
		}
	} else {
		recipient = `+234${recipient.substr(1)}`;
		num.push(recipient);
	}
	const client = new twilio(accountSid, authToken);
	return client.messages
		.create({
			body: message,
			to: num, // Text this number
			from: process.env.TWILIO_PHONE_NUMBER // From a valid Twilio number
		})
		.then(message => console.log(message.sid))
		.catch(error => console.log(error));
}

module.exports = {
	sendSMS
};
