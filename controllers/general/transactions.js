const {bindTo, execSilentCallback} = require('silent-promise')
const {sendTransactionNotificationToAdmin} = require('../admin/notification')
const constants = require('../../utils/constants')

async function creditAccount(
  accountType,
  accountId,
  paymentId,
  creditDate,
  amount,
  shortDescription,
  longDescription
) {
  return await recordTxForAccount(
    'credit',
    accountType,
    accountId,
    paymentId,
    creditDate,
    amount,
    shortDescription,
    longDescription
  )
}

async function debitAccount(
  accountType,
  accountId,
  paymentId,
  deditDate,
  amount,
  shortDescription,
  longDescription,
  nextBillDate
) {
  return await recordTxForAccount(
    'debit',
    accountType,
    accountId,
    paymentId,
    deditDate,
    amount,
    shortDescription,
    longDescription,
    nextBillDate
  )
}

async function recordTxForAccount(
  txType,
  accountType,
  accountId,
  paymentId,
  creditOrDebitDate,
  amount,
  shortDescription,
  longDescription,
  nextBillDate
) {
  // save credit record for organization and update wallet
  const newPaymentRecord = {
    _id: paymentId,
    entity: {
      id: accountId,
      type: accountType,
    },
    paymentDate: new Date(creditOrDebitDate).getTime(),
    processedDate: Date.now(),
    amount,
    txType,
    shortDescription,
    longDescription,
  }

  const db = require('../../config/db').getDatabase()
  const txCollection = db.collection(
    constants.database.collections.transactions
  )
  const [saveTxError] = await execSilentCallback(
    txCollection.insertOne,
    bindTo(txCollection)
  )(newPaymentRecord)
  saveTxError.ifError('Error saving transaction info').thenStopExecution()

  sendTransactionNotificationToAdmin(
    txType,
    amount,
    shortDescription,
    accountType
  )

  if (accountType === 'organization') {
    return await updateOrganizationWalletBalance(
      accountId,
      txType === 'debit' ? -amount : amount,
      nextBillDate
    )
  } else {
    return await updateUserWalletBalance(
      accountId,
      txType === 'debit' ? -amount : amount,
      nextBillDate
    )
  }
}

async function updateOrganizationWalletBalance(
  organizationId,
  increaseDecrease,
  nextBillDate
) {
  // set to active so next billing cycle can attempt to charge this account
  const updateFields = {subscriptionStatus: 'active'}
  if (nextBillDate) {
    // subscription billing was successful
    updateFields.nextBillDate = nextBillDate
    updateFields.accountStatus = 'enabled'
  }
  const update = {
    $inc: {walletBalance: increaseDecrease},
    $set: updateFields,
  }

  const findOrg = {_id: organizationId}
  const options = {
    returnOriginal: false,
    projection: {walletBalance: true},
  }

  const db = require('../../config/db').getDatabase()
  const orgCollection = db.collection(
    constants.database.collections.organizations
  )
  const [updateError, updateResult] = await execSilentCallback(
    orgCollection.findOneAndUpdate,
    bindTo(orgCollection)
  )(findOrg, update, options)
  updateError.ifError('Organization wallet update error').thenStopExecution()

  return updateResult.value.walletBalance
}

async function updateUserWalletBalance(userId, increaseDecrease, nextBillDate) {
  // set to active so next billing cycle can attempt to charge this account
  const updateFields = {subscriptionStatus: 'active'}
  if (nextBillDate) {
    // subscription billing was successful
    updateFields.nextBillDate = nextBillDate
    updateFields.accountStatus = 'enabled'
  }
  const update = {
    $inc: {walletBalance: increaseDecrease},
    $set: updateFields,
  }
  const findOrg = {_id: userId}
  const options = {
    returnOriginal: false,
    projection: {walletBalance: true},
  }

  const db = require('../../config/db').getDatabase()
  const usersCollection = db.collection(constants.database.collections.users)
  const [updateError, updateResult] = await execSilentCallback(
    usersCollection.findOneAndUpdate,
    bindTo(usersCollection)
  )(findOrg, update, options)
  updateError.ifError('User wallet update error').thenStopExecution()

  return updateResult.value.walletBalance
}

module.exports = {
  creditAccount,
  debitAccount,
}
