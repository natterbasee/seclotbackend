const redis = require("redis").createClient();
const { bindTo, execSilentCallback } = require("silent-promise");
const utils = require("../../utils/index");

const settingsRedisKey = "seclotSettings";
const defaultSettings = {
    referralAmount: 100,
    referralPercentage: 10,
    subscriptionPlans: {
        daily: {
            name: "Basic Plan", amount: 5, frequency: { minutes: 5 }
        },
        weekly: {
            name: "Amateur Plan", amount: 30, frequency: { weeks: 1 }
        },
        monthly: {
            name: "Pro Plan", amount: 100, frequency: { months: 1 }
        },
        yearly: {
            name: "Premium Plan", amount: 1000, frequency: { years: 1 }
        }
    }
};

async function getSettings(req, res) {
    let fields;
    if (req.query.fields) {
        fields = req.query.fields.split(",");
    }
    else {
        fields = Object.keys(defaultSettings);
    }

    const settings = {};
    for (const field of fields) {
        settings[field] = await getValue(field);
    }

    res.is.ok(settings);
}

async function updateSettings(req, res) {
    const fields = Object.keys(defaultSettings);
    if (utils.hasNoneOfTheRequiredParameters(req.body, fields, res)) {
        return;
    }

    if (req.body.referralAmount && typeof req.body.referralAmount !== "number") {
        res.is.badRequest(400, "Invalid update data. referralAmount should be a number");
        return;
    }

    if (req.body.referralPercentage && typeof req.body.referralPercentage !== "number") {
        res.is.badRequest(400, "Invalid update data. referralPercentage should be a number");
        return;
    }

    if (req.body.subscriptionPlans && typeof req.body.subscriptionPlans !== "object") {
        res.is.badRequest(400, "Invalid update data. subscriptionPlans should be an object");
        return;
    }

    if (req.body.subscriptionPlans) {
        const currentPlans = await getValue("subscriptionPlans");
        const updatePlans = currentPlans;

        for (const subField of Object.keys(currentPlans)) {
            const updatePlan = req.body.subscriptionPlans[subField];
            if (!updatePlan) {
                continue;
            }

            if (updatePlan.name && typeof updatePlan.name === "string") {
                updatePlans[subField].name = updatePlan.name;
            }
            if (updatePlan.amount && typeof updatePlan.amount === "number") {
                updatePlans[subField].amount = updatePlan.amount;
            }
        }

        req.body.subscriptionPlans = updatePlans;
    }

    const settings = {};
    for (const field of fields) {
        if (req.body[field]) {
            settings[field] = await setValue(field, req.body[field]);
        }
        else {
            settings[field] = await getValue(field);
        }
    }

    res.is.ok(settings);
}

async function deleteSettings(req, res) {
    if (utils.parametersAreIncomplete(req.query, ["fields"],res)) {
        return;
    }

    let fields = req.query.fields.split(",");
    for (const field of fields) {
        await deleteValue(field);
    }

    delete req.query.fields;
    await getSettings(req, res);
}

async function getValue(key) {
    key = key.trim();
    const [error, value] = await execSilentCallback(redis.hget, bindTo(redis))(settingsRedisKey, key);
    error.ifError(`settings: get ${key} from redis failed`).thenStopExecution();

    if (!value) {
        return defaultSettings[key];
    }

    try {
        return JSON.parse(value);
    }
    catch (error) {
        return value;
    }
}

async function setValue(key, newValue) {
    key = key.trim();
    let valueToSet = newValue;
    if (typeof newValue === "object" || newValue instanceof Array) {
        valueToSet = JSON.stringify(newValue);
    }

    [error] = await execSilentCallback(redis.hset, bindTo(redis))(settingsRedisKey, key, valueToSet);
    error.ifError(`settings: failed to set new value for ${key}`).thenStopExecution();

    return newValue;
}

async function deleteValue(key) {
    key = key.trim();
    const [error] = await execSilentCallback(redis.hdel, bindTo(redis))(settingsRedisKey, key);
    error.ifError(`settings: failed to delete ${key}`).thenStopExecution();
    return defaultSettings[key];
}

async function getSubscriptionPlans() {
    return await getValue("subscriptionPlans");
}

async function getReferralAmount() {
    return await getValue("referralAmount");
}

async function getReferralPercentage() {
    return await getValue("referralPercentage");
}

module.exports = {
    getSettings,
    updateSettings,
    deleteSettings,
    getSubscriptionPlans,
    getReferralAmount,
    getReferralPercentage
};