const FirebaseAuth = require("firebaseauth");
const { bindTo, execSilentCallback } = require("silent-promise");
const utils = require("../../utils");
const constants = require("../../utils/constants");

async function requestPasswordReset(req, res) {
    if (utils.parametersAreIncomplete(req.query, ["email"], res)) {
        return;
    }
    const email = req.query.email.toLowerCase();

    // check if email exists for admin
    if (await noSuchEmailForAdmin(email) && await noSuchEmailForOrganization(email)) {
        res.is.badRequest(400, "No account exists with that email");
        return;
    }

    const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
    utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
    const [authError] = await execSilentCallback(firebaseAuth.sendPasswordResetEmail, email);
    if (authError.getError()) {
        res.is.badRequest(401, authError.getError().message);
        return;
    }

    res.is.ok({ message: "Password reset email has been sent to the email address provided" });
}

async function noSuchEmailForAdmin(email) {
    const query = { email };
    const options = { projection: { email: true } };

    const db = require("../../config/db").getDatabase();
    const adminCollection = db.collection(constants.database.collections.admins);
    const [findError, admin] = await execSilentCallback(adminCollection.findOne, bindTo(adminCollection))(query, options);
    findError.ifError("Failed to check email in admin database").thenStopExecution();

    return !admin;
}

async function noSuchEmailForOrganization(email) {
    const query = { email };
    const options = { projection: { email: true } };

    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);
    const [findError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    findError.ifError("Failed to check email in organization database").thenStopExecution();

    return !org;
}

async function resetPassword(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["code", "password"], res)) {
        return;
    }

    if (req.body.password.length < 6) {
        res.is.badRequest(400, "Password must be 6 or more characters");
        return;
    }
    if (utils.passwordIsNotValid(req.body.password)) {
        res.is.badRequest(400, utils.passwordErrorMessage);
        return;
    }

    const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
    utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
    const [authError] = await execSilentCallback(firebaseAuth.resetPassword, req.body.code, req.body.password);
    if (authError.getError()) {
        res.is.badRequest(401, authError.getError().message);
        return;
    }

    res.is.ok({ message: "Password reset successful" });
}

async function refreshToken(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["refreshToken"], res)) {
        return;
    }

    const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY);
    utils.bindMethodsToObject(firebaseAuth, FirebaseAuth);
    const [authError, authResult] = await execSilentCallback(firebaseAuth.refreshToken, req.body.refreshToken);
    if (authError.getError()) {
        res.is.badRequest(401, authError.getError().message);
        return;
    }

    res.is.ok({
        refreshToken: authResult.refreshToken,
        token: authResult.token,
    });
}

async function getNextId() {
    const db = require("../../config/db").getDatabase();
    const collection = db.collection(constants.database.collections.accountId);

    const query = { _id: "seclot_id" };
    const incrementId = { $inc: { lastAssignedId: 1 } };
    const options = { upsert: true, returnOriginal: false };

    const [error, result] = await execSilentCallback(collection.findOneAndUpdate, bindTo(collection))(query, incrementId, options);
    error.ifError("Failed to increment seclot id").thenStopExecution();

    return result.value.lastAssignedId;
}

async function getAccountIdWithSeclotId(seclotId) {
    const db = require("../../config/db").getDatabase();

    const orgCollection = db.collection(constants.database.collections.organizations);
    const [findOrgError, organization] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))({ seclotId });
    findOrgError.ifError("Error checking seclot id").thenStopExecution();

    if (organization) {
        return { id: organization._id, type: "organization", profile: organization };
    }

    const usersCollection = db.collection(constants.database.collections.users);
    const [findUserError, user] = await execSilentCallback(usersCollection.findOne, bindTo(usersCollection))({ seclotId });
    findUserError.ifError("Error checking seclot id").thenStopExecution();

    if (user) {
        return { id: user._id, type: "user", profile: user };
    }
}

module.exports = {
    requestPasswordReset,
    resetPassword,
    refreshToken,
    getNextId,
    getAccountIdWithSeclotId
};