const {bindTo, execSilentCallback, silentPromise} = require('silent-promise');
const FirebaseAdmin = require('firebase-admin');
const {logError} = require('../../utils');

const firebaseServiceAccountJson = require('../../seclot-firebase.json');
const firebaseInitOptions = {
  credential: FirebaseAdmin.credential.cert(firebaseServiceAccountJson),
  databaseURL: `https://${firebaseServiceAccountJson.project_id}.firebaseio.com`
};
const firebaseApp = FirebaseAdmin.initializeApp(
  firebaseInitOptions,
  'databaseApp'
);

async function sendNotificationToApp(
  notificationIds,
  subject,
  message,
  account
) {
  console.log('here', notificationIds, subject);
  // while we're here, let's save the notification to firebase
  const dbRef = FirebaseAdmin.database(firebaseApp).ref(account.id);
  const time = Date.now();
  dbRef
    .child('notifications')
    .push({subject, message, time, sort: -time})
    .catch(logError);

  dbRef
    .child('newNotifications')
    .transaction(counter => (counter || 0) + 1)
    .catch(logError);

  const opResult = {sentToDevices: 0, failedDevices: 0};
  if (
    !notificationIds ||
    !(notificationIds instanceof Array) ||
    notificationIds.length === 0
  ) {
    return opResult;
  }

  const payload = {
    notification: {
      title: subject,
      body: message
    }
  };

  const [error, fcmResult] = await silentPromise(
    FirebaseAdmin.messaging().sendToDevice(notificationIds, payload)
  );
  error.ifError('Send notification failed!').thenStopExecution();

  console.log('here', error, fcmResult);

  // check if invalid token exists in result error and remove from profile
  const invalidTokens = [];
  const invalidTokenErrorCodes = [
    'messaging/invalid-registration-token',
    'messaging/registration-token-not-registered',
    'messaging/mismatched-credential'
  ];
  for (let i = 0; i < fcmResult.results.length; i++) {
    const deviceResult = fcmResult.results[i];
    if (
      deviceResult.error &&
      invalidTokenErrorCodes.indexOf(deviceResult.error.code) >= 0
    ) {
      console.log(deviceResult.error.code, notificationIds[i]);
      invalidTokens.push(notificationIds[i]);
    }
  }

  if (invalidTokens.length > 0) {
    // remove from account
    const db = require('../../config/db').getDatabase();
    const orgCollection = db.collection(account.collection);

    const query = {_id: account.id};
    const update = {
      $pullAll: {notificationIds: invalidTokens}
    };

    await execSilentCallback(orgCollection.updateOne, bindTo(orgCollection))(
      query,
      update
    );
  }

  opResult.sentToDevices = fcmResult.successCount;
  opResult.failedDevices = fcmResult.failureCount;

  return opResult;
}

module.exports = {
  sendNotificationToApp
};
