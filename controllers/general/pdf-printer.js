const pdfMakePrinter = require('pdfmake/src/printer');
const { join } = require("path");

const fonts = {
  Roboto: {
    normal: join(__dirname, './fonts/Roboto-Regular.ttf'),
    bold: join(__dirname, './fonts/Roboto-Medium.ttf'),
    italics: join(__dirname, './fonts/Roboto-Italic.ttf'),
    bolditalics: join(__dirname, './fonts/Roboto-MediumItalic.ttf')
  }
};

module.exports = function createPDFDoc(title, subtitle, rowHeader, rows, columnWidths) {
  const docDefinition = {
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        margin: [0, 0, 0, 5]
      },
      subHeader: {
        bold: true,
        color: 'gray',
        margin: [0, 5, 0, 10]
      },
      table: {
        margin: [0, 5, 0, 15]
      },
      tableHeader: {
        bold: true,
        fontSize: 13,
        color: 'black'
      }
    },
    content: [
      {
        text: title,
        style: 'header'
      },
      {
        text: subtitle,
        style: 'subHeader'
      },
      {
        table: {
          headerRows: 1,
          widths: columnWidths,
          body: [
            rowHeader, ...rows
          ]
        },
        style: "table"
      }
    ]
  };

  const printer = new pdfMakePrinter(fonts);
  return printer.createPdfKitDocument(docDefinition);
};