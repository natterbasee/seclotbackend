const redis = require("redis").createClient();
const { bindTo, execSilentCallback } = require("silent-promise");
const utils = require("../../utils/index");

const sectorsRedisKey = "seclotOrganizationSectors";

async function getSectors(req, res) {
    const [error, values] = await execSilentCallback(redis.smembers, bindTo(redis))(sectorsRedisKey);
    error.ifError(`org sectors: get values from redis failed`).thenStopExecution();

    res.is.ok(values);
}

async function addSectors(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["sectors"], res)) {
        return;
    }

    let sectors = req.body.sectors;
    if (!(sectors instanceof Array) || sectors.length === 0) {
        res.is.badRequest(400, "sectors must be an array of at least 1 sector to add");
        return;
    }

    if (sectors.some(sector => typeof sector !== "string")) {
        res.is.badRequest(400, "All sectors to add must be string values");
        return;
    }

    sectors = sectors.map(sector => sector.trim());
    [error] = await execSilentCallback(redis.sadd, bindTo(redis))(sectorsRedisKey, sectors);
    error.ifError(`org sectors: failed to add ${sectors}`).thenStopExecution();

    await getSectors(req, res);
}

async function deleteSectors(req, res) {
    if (utils.parametersAreIncomplete(req.query, ["sectors"],res)) {
        return;
    }

    let sectors = req.query.sectors.split(",");
    if (sectors.some(sector => typeof sector !== "string")) {
        res.is.badRequest(400, "All sectors to delete must be string values");
        return;
    }

    sectors = sectors.map(sector => sector.trim());
    [error] = await execSilentCallback(redis.srem, bindTo(redis))(sectorsRedisKey, sectors);
    error.ifError(`org sectors: failed to delete ${sectors}`).thenStopExecution();

    await getSectors(req, res);
}

module.exports = {
    getSectors,
    addSectors,
    deleteSectors
};