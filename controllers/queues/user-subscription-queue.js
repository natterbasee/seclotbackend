const { bindTo, execSilentCallback, SilentPromiseError } = require("silent-promise");
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const constants = require("../../utils/constants");
const settings = require("../../controllers/general/settings");
const logger = require("../../utils/logger");
const { sendNotificationToApp } = require("../general/notifications");
const { debitAccount } = require("../general/transactions");

const _10_seconds = 10 * 1000;

function processUserSubscriptionsRecurrently() {
    processUserSubscriptions()
        .then(() => {
            setTimeout(processUserSubscriptionsRecurrently, _10_seconds);
        })
        .catch(logError);
}

async function processUserSubscriptions() {
    const db = require("../../config/db").getDatabase();
    const usersCollection = db.collection(constants.database.collections.users);

    // get all organizations whose nextBillingDate is in the past
    const query = {
        subscriptionStatus: "active",
        nextBillDate: { $lt: Date.now() }
    };
    const options = { projection: { plan: true, walletBalance: true, notificationIds: true } };
    const [error, cursor] = await execSilentCallback(usersCollection.find, bindTo(usersCollection))(query, options);
    error.ifError("Failed to find user's with pending subscription debit").thenStopExecution();

    const plans = await settings.getSubscriptionPlans();

    while (await cursor.hasNext()) {
        const user = await cursor.next();
        let selectedPlan;
        for (const planName of Object.keys(plans)) {
            if (planName.toLowerCase() === user.plan.toLowerCase()) {
                selectedPlan = plans[planName];
                break;
            }
        }

        const nextBill = selectedPlan.amount;
        if (!user.walletBalance || user.walletBalance < nextBill) {
            disableUser(user, usersCollection).catch(logError);
        }
        else {
            billUser(nextBill, selectedPlan.frequency, user).catch(logError);
        }
    }
}

async function disableUser(user, usersCollection) {
    const thisAccount = { id: user._id, collection: constants.database.collections.users };
    sendNotificationToApp(user.notificationIds, "Account disabled",
        "Your wallet balance was not enough to cover your last bill. Please top up your wallet soonest to reactivate your account", thisAccount)
        .catch(logError);

    const userUpdate = {
        $set: { subscriptionStatus: "inactive", accountStatus: "disabled" }
    };

    const query = { _id: user._id };
    const [updateError] = await execSilentCallback(usersCollection.updateOne, bindTo(usersCollection))(query, userUpdate);
    updateError.ifError("Error disabling user account because of failed payment").thenStopExecution();
}

async function billUser(billAmount, billingFrequency, user) {
    if (user.walletBalance < billAmount * 5) {
        const thisAccount = { id: user._id, collection: constants.database.collections.users };
        sendNotificationToApp(user.notificationIds, "Wallet balance running low",
            "Please top up your wallet soonest so that your account will not be disabled", thisAccount).catch(logError);
    }

    const txId = new ObjectID().toHexString();
    const shortDescription = `Subscription (${user.plan} plan)`,
        longDescription = `Subscription charge for ${user.plan} plan`;
    const nextBillDate = moment().add(billingFrequency).valueOf();
    await debitAccount("user", user._id, txId, Date.now(), billAmount, shortDescription, longDescription, nextBillDate);
}

function logError(error) {
    let detail;
    if (error instanceof SilentPromiseError) {
        detail = error.error;
    }
    else {
        detail = error.stack;
    }

    logger.error({ detail, path: "user-subscription-queue" }, error.message);
}

module.exports = {
    processUserSubscriptionsRecurrently
};