const { bindTo, execSilentCallback, SilentPromiseError } = require("silent-promise");
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const cron = require("node-cron");
const constants = require("../../utils/constants");
const settings = require("../general/settings");
const { getAccountIdWithSeclotId } = require("../general/auth");
const { creditAccount } = require("../general/transactions");

function runOrganizationReferralCreditCronJob() {
    cron.schedule("* * 21 * *", creditOrganizationsReferralSync, {});
}

function creditOrganizationsReferralSync() {
    creditOrganizationReferrals()
        .catch(error => {
            let detail;
            if (error instanceof SilentPromiseError) {
                detail = error.error;
            }
            else {
                detail = error.stack;
            }

            logger.error({ detail, path: "subscription-queue" }, error.message);
        });
}

async function creditOrganizationReferrals() {
    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);
    const txCollection = db.collection(constants.database.collections.transactions);

    // get array of referral ids and their referred organization id
    const query = {
        referralId: { $exists: true }
    };
    const options = { projection: { name: true, referralId: true, lastReferralCreditDate: true } };
    const [error, cursor] = await execSilentCallback(orgCollection.find, bindTo(orgCollection))(query, options);
    error.ifError("Error fetching organization referrals to credit at month end").thenStopExecution();

    const processed = [];
    while (await cursor.hasNext()) {
        const organization = await cursor.next();
        const txs = await getRecentTxForOrganization(txCollection, organization._id, organization.lastReferralCreditDate || 0);
        if (txs.length === 0) {
            processed.push({ organization, referralEarning: 0 });
            continue;
        }

        let totalAmount = 0;
        for (const tx of txs) {
            totalAmount += tx.amount;
        }

        const referralPercentage = await settings.getReferralPercentage();
        const referralEarning = totalAmount * referralPercentage / 100;
        const referral = await getAccountIdWithSeclotId(organization.referralId);

        let lastReferralCreditDateString;
        if (!organization.lastReferralCreditDate) {
            lastReferralCreditDateString = "account creation date";
        }
        else {
            lastReferralCreditDateString = moment(organization.lastReferralCreditDate).format("dddd, MMMM Do YYYY");
        }

        const newLastReferralCreditDate = Date.now(),
            newLastReferralCreditDateString = moment(newLastReferralCreditDate).format("dddd, MMMM Do YYYY");

        const txId = new ObjectID().toHexString();
        const shortDescription = "Referral Earning",
            longDescription = `From organization (${organization.name}) for ${lastReferralCreditDateString} to ${newLastReferralCreditDateString}`;
        await creditAccount(referral.type, referral.id, txId, newLastReferralCreditDate, referralEarning, shortDescription, longDescription);

        // set newLastReferralCreditDate for organization
        const update = { $set: { lastReferralCreditDate: newLastReferralCreditDate } };
        const [updateOrgError] = await execSilentCallback(orgCollection.updateOne, bindTo(orgCollection))({ _id: organization._id }, update);
        updateOrgError.ifError("Organization lastReferralCreditDate update error").thenStopExecution();

        processed.push({ organization, referralEarning });
    }

    return processed;
}

// get debit transactions for organization between last credit date and now
async function getRecentTxForOrganization(txCollection, organizationId, lastReferralCreditDate) {
    const query = { "entity.id": organizationId, txType: "debit", processedDate: { $gt: lastReferralCreditDate } };
    const options = { projection: { amount: true } };
    const [error, cursor] = await execSilentCallback(txCollection.find, bindTo(txCollection))(query, options);
    error.ifError("Error getting organization transactions to credit referral").thenStopExecution();

    return cursor.toArray();
}

module.exports = {
    runOrganizationReferralCreditCronJob,
    creditOrganizationReferrals
};