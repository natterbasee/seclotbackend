const { bindTo, execSilentCallback, SilentPromiseError } = require("silent-promise");
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const constants = require("../../utils/constants");
const settings = require("../../controllers/general/settings");
const logger = require("../../utils/logger");
const { sendNotificationToApp } = require("../general/notifications");
const { debitAccount } = require("../general/transactions");

const _10_seconds = 10 * 1000;

function processOrganizationSubscriptionsRecurrently() {
    processOrganizationSubscriptions()
        .then(() => {
            setTimeout(processOrganizationSubscriptionsRecurrently, _10_seconds);
        })
        .catch(logError);
}

async function processOrganizationSubscriptions() {
    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    // get all organizations whose nextBillingDate is in the past
    const query = {
        subscriptionStatus: "active",
        nextBillDate: { $lt: Date.now() }
    };
    const options = { projection: { plan: true, walletBalance: true, beneficiaries: true, disabledBeneficiaries: true, notificationIds: true } };
    const [error, cursor] = await execSilentCallback(orgCollection.find, bindTo(orgCollection))(query, options);
    error.ifError("Failed to update organization's subscription plan").thenStopExecution();

    const plans = await settings.getSubscriptionPlans();

    while (await cursor.hasNext()) {
        const organization = await cursor.next();
        let selectedPlan;
        for (const planName of Object.keys(plans)) {
            if (planName.toLowerCase() === organization.plan.toLowerCase()) {
                selectedPlan = plans[planName];
                break;
            }
        }

        const totalBillableUsersInOrg = (organization.beneficiaries || []).length + (organization.disabledBeneficiaries || []).length;
        const nextBill = totalBillableUsersInOrg * selectedPlan.amount;
        if (!organization.walletBalance || organization.walletBalance < nextBill) {
            disableOrganization(organization, orgCollection).catch(logError);
        }
        else {
            billOrganization(nextBill, totalBillableUsersInOrg, selectedPlan.frequency, organization).catch(logError);
        }
    }
}

async function disableOrganization(organization, orgCollection) {
    const thisAccount = { id: organization._id, collection: constants.database.collections.organizations };
    sendNotificationToApp(organization.notificationIds, "Account disabled",
        "Your wallet balance was not enough to cover your last bill. Please top up your wallet soonest to reactivate your account", thisAccount)
        .catch(logError);

    const orgUpdate = {
        $set: { subscriptionStatus: "inactive", accountStatus: "disabled" }
    };

    const query = { _id: organization._id };
    const [updateError] = await execSilentCallback(orgCollection.updateOne, bindTo(orgCollection))(query, orgUpdate);
    updateError.ifError("Error disabling organization because of failed payment").thenStopExecution();
}

async function billOrganization(billAmount, totalBillableUsersInOrg, billingFrequency, organization) {
    if (organization.walletBalance < billAmount * 5) {
        const thisAccount = { id: organization._id, collection: constants.database.collections.organizations };
        sendNotificationToApp(organization.notificationIds, "Wallet balance running low",
            "Please top up your wallet soonest so that your account will not be disabled", thisAccount).catch(logError);
    }

    const txId = new ObjectID().toHexString();
    const shortDescription = `Subscription (${organization.plan} plan)`,
        longDescription = `Subscription charge for ${totalBillableUsersInOrg} beneficiaries`;
    const nextBillDate = moment().add(billingFrequency).valueOf();
    await debitAccount("organization", organization._id, txId, Date.now(), billAmount, shortDescription, longDescription, nextBillDate);
}

function logError(error) {
    let detail;
    if (error instanceof SilentPromiseError) {
        detail = error.error;
    }
    else {
        detail = error.stack;
    }

    logger.error({ detail, path: "org-subscription-queue" }, error.message);
}

module.exports = {
    processOrganizationSubscriptionsRecurrently
};