const rp = require('request-promise');
const { bindTo, execSilentCallback, silentPromise } = require("silent-promise");
const utils = require("../../utils");
const constants = require("../../utils/constants");
const { creditAccount } = require("../general/transactions");

async function fundWallet(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["transactionReference"], res)) {
        return;
    }

    const requestOptions = {
        uri: `https://api.paystack.co/transaction/verify/${req.body.transactionReference}`,
        headers: {
            'Authorization': `Bearer ${process.env.PAYSTACK_SECRET_KEY}`
        },
        simple: false,
        json: true
    };
    const [error, result] = await silentPromise(rp(requestOptions));
    error.ifError("Error processing payment").thenStopExecution();

    if (!result.data) {
        // transaction not validated
        res.is.badRequest(400, result.message || "Invalid transaction");
        return;
    }
    else if (result.data.status !== "success") {
        res.is.badRequest(400, `Payment was unsuccessful: ${result.data.gateway_response}`);
        return;
    }

    const tx = result.data;
    const db = require("../../config/db").getDatabase();
    const txCollection = db.collection(constants.database.collections.transactions);

    // check if payment has been processed before
    const checkRef = { _id: tx.reference };
    const [checkError, paymentRecord] = await execSilentCallback(txCollection.findOne, bindTo(txCollection))(checkRef);
    checkError.ifError("Error while validating payment").thenStopExecution();

    if (paymentRecord) {
        res.is.badRequest(400, "This payment has already been processed");
        return;
    }

    const paymentMethod = tx.authorization;
    const amount = tx.amount / 100;

    // process payment
    const shortDescription = "Wallet Topup",
        longDescription = `Wallet top up from ${paymentMethod.card_type.toUpperCase()} ${paymentMethod.channel.toUpperCase()} (****${paymentMethod.last4})`;
    const walletBalance = await creditAccount("organization", req.organization.id, tx.reference, new Date(tx.paidAt).getTime(),
        amount, shortDescription, longDescription);
    res.is.ok({ topup: amount, walletBalance });
}

module.exports = {
    fundWallet
};