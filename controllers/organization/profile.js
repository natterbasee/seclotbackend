const FirebaseAdmin = require("firebase-admin");
const multer = require("multer");
const { join, extname } = require("path");
const { unlinkSync } = require("fs");
const { bindTo, execSilentCallback, silentPromise } = require("silent-promise");
const utils = require("../../utils");
const constants = require("../../utils/constants");
const logger = require("../../utils/logger");
const { getAccountIdWithSeclotId } = require("../general/auth");
const settings = require("../general/settings");

async function getProfile(req, res) {
  const profile = req.organization;
  const organizationPlan = await getActiveOrganizationPlan(req.organization.plan);

  if (organizationPlan) {
    // get beneficiaries from db to estimate next bill amount
    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    const query = { _id: req.organization.id };
    const options = { projection: { plan: true, walletBalance: true, beneficiaries: true, disabledBeneficiaries: true } };
    const [error, organization] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    error.ifError("Error getting organization's profile from database").thenStopExecution();



    const totalBillableUsersInOrg = (organization.beneficiaries || []).length + (organization.disabledBeneficiaries || []).length;
    profile.nextBillAmount = totalBillableUsersInOrg * organizationPlan.amount;
    profile.planName = organizationPlan.name;
  }

  res.is.ok(utils.getFormattedProfile(profile, req));
}

async function getActiveOrganizationPlan(orgPlan) {
  if (!orgPlan) {
    return;
  }

  const plans = await settings.getSubscriptionPlans();
  for (const planName of Object.keys(plans)) {
    if (planName.toLowerCase() === orgPlan.toLowerCase()) {
      return plans[planName];
    }
  }
}

async function updateProfile(req, res) {
    const updateFields = ["name", "email", "password", "phoneNumber", "location", "referralId"];
    if (utils.hasNoneOfTheRequiredParameters(req.body, updateFields, res)) {
        return;
    }

    if (req.body.password) {
        if (req.body.password.length < 6) {
            res.is.badRequest(400, "Password must be 6 or more characters");
            return;
        }
        if (utils.passwordIsNotValid(req.body.password)) {
            res.is.badRequest(400, utils.passwordErrorMessage);
            return;
        }
    }

    if (req.body.email || req.body.password) {
        const newInfo = {
            email: req.body.email,
            password: req.body.password
        };
        const [firebaseError] = await silentPromise(FirebaseAdmin.auth().updateUser(req.organization.id, newInfo));
        if (firebaseError.getError()) {
            res.is.badRequest(400, firebaseError.getError().message);
            return;
        }
    }

    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    const query = { _id: req.organization.id };

  // check if referralId was already set, then remove from update list
  if (req.organization.referralId) {
    delete req.body.referralId;
  }

    if (req.body.referralId) {
      req.body.referralId = +req.body.referralId;

        // check if trying to refer self
        if (req.body.referralId === req.organization.seclotId) {
            res.is.badRequest(400, "Invalid referralId. Cannot refer yourself!");
            return;
        }

        // first time setting referralId, check if referral is valid
        const referralAccount = await getAccountIdWithSeclotId(req.body.referralId);
        if (!referralAccount) {
            res.is.badRequest(400, "referralId is not valid");
            return;
        }
    }

    const update = {};
    for (const field of updateFields) {
        if (req.body[field] && field !== "password") {
            update[field] = req.body[field];
        }
    }

    if (Object.keys(update).length === 0) {
        // nothing else to update, it's safe to return the profile in req.organization
        res.is.ok(utils.getFormattedProfile(req.organization, req));
        return;
    }

    // update other fields passed in
    const updateProfile = { $set: update };
    const updateOptions = {
        projection: { ices: false, paymentMethods: false, referralId: false, transactions: false },
        returnOriginal: false
    };

    const [updateError, updateResult] =
        await execSilentCallback(orgCollection.findOneAndUpdate, bindTo(orgCollection))(query, updateProfile, updateOptions);
    updateError.ifError("Failed to update organization profile on database").thenStopExecution();

    res.is.ok(utils.getFormattedProfile(updateResult.value, req));
}

async function updateProfilePicture(req, res) {
    const [uploadError] = await execSilentCallback(getMulterInstance().single("image"), req, res);
    const error = uploadError.getError();
    if (error) {
        switch (error.code) {
            case "LIMIT_UNEXPECTED_FILE":
                res.is.badRequest(400, `Expected 'image' field, got '${error.field}'`);
                break;

            default:
                logger.error({ detail: error, path: req.path, method: req.method }, "Profile picture upload error");
                res.is.serverError("Error processing file");
                break;
        }
        return;
    }

    if (!req.file) {
        return res.is.badRequest(400, "Missing file parameter: image");
    }

    const updateProfile = { $set: { picture: req.file.filename } };
    const updateOptions = {
        projection: { ices: false, paymentMethods: false, referralId: false, transactions: false },
        returnOriginal: false
    };

    const query = { _id: req.organization.id };
    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);
    const [updateError, updateResult] =
        await execSilentCallback(orgCollection.findOneAndUpdate, bindTo(orgCollection))(query, updateProfile, updateOptions);
    updateError.ifError("Failed to update organization profile on database").thenStopExecution();

    // delete previous image file
    const previousProfilePictureFileName = req.organization.picture;
    if (previousProfilePictureFileName) {
        const path = join(process.env.UPLOAD_PATH, previousProfilePictureFileName);
        unlinkSync(path);
    }

    res.is.ok(utils.getFormattedProfile(updateResult.value, req));
}

function getMulterInstance() {
    const profileImageStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(undefined, process.env.UPLOAD_PATH);
        },
        filename: (req, file, cb) => {
            cb(undefined, `profile_image_${Date.now()}${extname(file.originalname)}`);
        }
    });
    const imageFilter = (req, file, cb) => {
        cb(undefined, file.mimetype.startsWith("image"));
    };

    return multer({ storage: profileImageStorage, fileFilter: imageFilter, limits: { fieldSize: 2 } });
}

module.exports = {
    getProfile,
    updateProfile,
    updateProfilePicture
};