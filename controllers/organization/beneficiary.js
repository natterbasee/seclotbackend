const { bindTo, execSilentCallback } = require("silent-promise");
const bcrypt = require('bcrypt');
const utils = require("../../utils");
const constants = require("../../utils/constants");
const { getNextId } = require("../general/auth");
const { sendSMS } = require("../general/messaging");
const crypto = require("crypto")

async function getBeneficiaries(req, res) {
    if (req.query.iceID || req.query.icePhoneNumber) {
        await getBeneficiariesForICE(req, res);
    }
    else {
        const beneficiaries = await getAllBeneficiaries(req.organization.id);
        res.is.ok(beneficiaries);
    }
}

async function getBeneficiariesForICE(req, res) {
    const db = require("../../config/db").getDatabase();

    // first check if ice id/phone number belongs to this organization
    let query = {
        "addedBy.accountId": req.organization.id,
        "addedBy.accountType": "organization"
    };
    if (req.query.iceID) {
        query._id = req.query.iceID;
    }
    else {
        query.phoneNumber = req.query.icePhoneNumber;
    }
    const options = { projection: { name: true, phoneNumber: true } };

    const iceCollection = db.collection(constants.database.collections.ices);
    const [findICEError, ice] = await execSilentCallback(iceCollection.findOne, bindTo(iceCollection))(query, options);
    findICEError.ifError("failed to get ice info from database").thenStopExecution();

    if (!ice) {
        res.is.badRequest(400, `Invalid ICE ${req.query.iceID ? 'ID' : 'Phone Number'}. No such ICE exist for this organization`);
        return;
    }

    const iceID = ice._id;

    // get beneficiary IDs from user_ice link
    const userICEsCollection = db.collection(constants.database.collections.user_ices);
    options.projection = { userId: true, status: true, assignDate: true };
    const [findError, iceLinkCursor] = await execSilentCallback(userICEsCollection.find, bindTo(userICEsCollection))({ iceID }, options);
    findError.ifError("failed to get beneficiaries for ICE from database").thenStopExecution();

    const userICEsLink = await iceLinkCursor.toArray();
    if (userICEsLink.length === 0) {
        res.is.ok([]);
        return;
    }

    const userICE = {};
    const beneficiaryIds = [];
    for (const link of userICEsLink) {
        const userId = link.userId;
        beneficiaryIds.push(userId);

        delete link._id;
        delete link.userId;
        link.name = ice.name;
        link.phoneNumber = ice.phoneNumber;

        userICE[userId] = link;
    }
    query = {
        _id: { $in: beneficiaryIds }
    };
    options.projection = { firstName: true, lastName: true, phoneNumber: true };
    const usersCollection = db.collection(constants.database.collections.users);
    const [getBeneficiariesError, usersCursor] = await execSilentCallback(usersCollection.find, bindTo(usersCollection))(query, options);
    getBeneficiariesError.ifError("Failed to get beneficiaries info from database").thenStopExecution();

    let beneficiaries = await usersCursor.toArray();

    // get organization enabled and disabled beneficiaries to cross check
    query = { _id: req.organization.id };
    options.projection = { beneficiaries: true, disabledBeneficiaries: true };
    const orgCollection = db.collection(constants.database.collections.organizations);
    const [getOrgError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    getOrgError.ifError("Failed to get organization's beneficiaries list from database").thenStopExecution();

    beneficiaries = beneficiaries.map(beneficiary => {
        if (org.disabledBeneficiaries) {
            beneficiary.organizationDisabled = org.disabledBeneficiaries.indexOf(beneficiary._id) >= 0;
        }
        else {
            beneficiary.organizationDisabled = false;
        }

        beneficiary.ice = userICE[beneficiary._id];

        beneficiary.phoneNumber = beneficiary._id;
        delete beneficiary._id;

        return beneficiary;
    });

    res.is.ok(beneficiaries);
}

async function getAllBeneficiaries(organizationId) {
    const db = require("../../config/db").getDatabase();

    let query = { _id: organizationId };
    let options = { projection: { beneficiaries: true, disabledBeneficiaries: true } };
    const orgCollection = db.collection(constants.database.collections.organizations);
    const [getOrgError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    getOrgError.ifError("Failed to get beneficiaries list from database").thenStopExecution();

    const beneficiaryIds = [];
    if (org.beneficiaries) {
        beneficiaryIds.push(...org.beneficiaries);
    }
    if (org.disabledBeneficiaries) {
        beneficiaryIds.push(...org.disabledBeneficiaries);
    }

    if (beneficiaryIds.length === 0) {
        return [];
    }

    // get detailed ice info for these beneficiaries in the organization's profile
    const beneficiariesICE = await getBeneficiariesICEs(db, beneficiaryIds, organizationId);

    // get beneficiary info from users database
    query = {
        _id: { $in: beneficiaryIds }
    };
    options = { projection: { firstName: true, lastName: true, phoneNumber: true, location: true, lastLocationUpdate: true } };
    const usersCollection = db.collection(constants.database.collections.users);
    const [getBeneficiariesError, cursor] = await execSilentCallback(usersCollection.find, bindTo(usersCollection))(query, options);
    getBeneficiariesError.ifError("Failed to get beneficiaries info from database").thenStopExecution();

    let beneficiaries = await cursor.toArray();

    return beneficiaries.map(beneficiary => {
        if (org.disabledBeneficiaries) {
            beneficiary.organizationDisabled = org.disabledBeneficiaries.indexOf(beneficiary._id) >= 0;
        }
        else {
            beneficiary.organizationDisabled = false;
        }

        beneficiary.ice = beneficiariesICE[beneficiary._id];
        beneficiary.phoneNumber = beneficiary._id;
        delete beneficiary._id;

        if (beneficiary.location) {
            beneficiary.lastKnownLocation = {
                longitude: beneficiary.location.coordinates[0],
                latitude: beneficiary.location.coordinates[1]
            };
            delete beneficiary.location;
        }

        return beneficiary;
    });
}

async function getBeneficiariesICEs(db, beneficiaryIds, organizationId) {
    // get beneficiary ices (id, status and date assigned) from user-ices database
    const beneficiaryICEIDs = beneficiaryIds.map(id => `${id}_${organizationId}`);
    let query = {
        _id: { $in: beneficiaryICEIDs }
    };
    let options = { projection: { userId: true, iceID: true, status: true, assignDate: true } };
    const userICEsCollection = db.collection(constants.database.collections.user_ices);
    const [getBeneficiaryICEsError, beneficiaryICEsCursor] = await execSilentCallback(userICEsCollection.find, bindTo(userICEsCollection))(query, options);
    getBeneficiaryICEsError.ifError("Failed to get beneficiaries ices from database").thenStopExecution();

    const beneficiaryICEs = await beneficiaryICEsCursor.toArray();

    // get ice info (name and phone number) from ice database
    const iceIDs = beneficiaryICEs.map(ice => ice.iceID);
    query = {
        _id: { $in: iceIDs }
    };
    options = { projection: { name: true, phoneNumber: true } };
    const iceCollection = db.collection(constants.database.collections.ices);
    const [getICEsError, icesCursor] = await execSilentCallback(iceCollection.find, bindTo(iceCollection))(query, options);
    getICEsError.ifError("Failed to get detailed beneficiaries ices info from database").thenStopExecution();

    const icesInfo = await icesCursor.toArray();
    const icesInfoObj = {};
    for (const iceInfo of icesInfo) {
        icesInfoObj[iceInfo._id] = { name: iceInfo.name, phoneNumber: iceInfo.phoneNumber };
    }

    const beneficiariesICEsObject = {};
    for (const ice of beneficiaryICEs) {
        const iceId = ice.iceID, beneficiaryId = ice.userId;
        delete ice._id;
        delete ice.iceID;

        ice.name = icesInfoObj[iceId] ? icesInfoObj[iceId].name : "";
        ice.phoneNumber = ice.userId;
        delete ice.userId;
        beneficiariesICEsObject[beneficiaryId] = ice;
    }

    return beneficiariesICEsObject;
}

async function addBeneficiaries(req, res) {


    if (utils.parametersAreIncomplete(req.body, ["beneficiaries"], res)) {
        return;
    }

    const beneficiaries = req.body.beneficiaries;
    if (!(beneficiaries instanceof Array)) {
        res.is.badRequest(400, `Expected an array, got '${typeof req.body.beneficiaries}'`);
        return;
    }
    if (beneficiaries.length === 0) {
        res.is.badRequest(400, "Provide at least one beneficiary information to add");
        return;
    }

    const db = require("../../config/db").getDatabase();

    const orgCollection = db.collection(constants.database.collections.organizations);
    const usersCollection = db.collection(constants.database.collections.users);
    const userICEsCollection = db.collection(constants.database.collections.user_ices);

    const query = { _id: req.organization.id };
    let options = { projection: { beneficiaries: true, disabledBeneficiaries: true } };
    const [getOrgError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    getOrgError.ifError("Failed to get current beneficiaries list from database").thenStopExecution();

    const userIDs = [];
    const usersBatchUpload = usersCollection.initializeUnorderedBulkOp();
    const userICEsBatchUpload = userICEsCollection.initializeUnorderedBulkOp();

    const date = Date.now();
    let hasInvalidDataCount = 0, hasInvalidBeneficiariesCount = 0;
    const duplicateBeneficiaries = [];

    for (let i = 0; i < beneficiaries.length; i++) {
        const beneficiary = validateBeneficiary(beneficiaries[i]);
        if (!beneficiary) {
            hasInvalidDataCount++;
            continue;
        }

        const iceID = await getValidBeneficiaryIceId(db, req.organization.id, beneficiary);
        if (!iceID) {
            hasInvalidBeneficiariesCount++;
            continue;
        }

        if (userIDs.indexOf(beneficiary.phoneNumber) >= 0 ||
            (org.beneficiaries || []).indexOf(beneficiary.phoneNumber) >= 0 ||
            (org.disabledBeneficiaries || []).indexOf(beneficiary.phoneNumber) >= 0) {
            duplicateBeneficiaries.push(beneficiary.phoneNumber);
            continue;
        }

        userIDs.push(beneficiary.phoneNumber);
        let pin = Math.floor(Math.random() * 8999 + 1000).toString()
        const newSeclotId = await getNextId();
        usersBatchUpload.find({ _id: beneficiary.phoneNumber }).upsert().updateOne({
            $setOnInsert: {
                firstName: beneficiary.firstName, lastName: beneficiary.lastName,
                createdBy: req.user.id,
                pinHash: bcrypt.hashSync(pin, 10), 
                seclotId: newSeclotId, 
                accountCreationDate: date
            }
        });

        userICEsBatchUpload.find({ _id: `${beneficiary.phoneNumber}_${req.organization.id}` }).upsert().updateOne({
            $setOnInsert: { userId: beneficiary.phoneNumber, status: "active", assignDate: date },
            $set: { iceID }
        });

        const message = `${req.organization.name} just added you as a Beneficiary on Seclot. Download the Seclot app and login to connect. Your pin is ${pin}`;
        await sendSMS(userIDs, message).catch(console.error);
    }

    if (userIDs.length === 0) {
        const errors = [];
        if (hasInvalidDataCount > 0) {
            errors.push(`${hasInvalidDataCount} beneficiary info has missing or invalid name, phoneNumber and/or iceIDs`)
        }
        if (hasInvalidBeneficiariesCount > 0) {
            errors.push(`${hasInvalidBeneficiariesCount} beneficiaries have no valid ICE groups assigned to them`)
        }
        if (duplicateBeneficiaries.length > 0) {
            errors.push(`found duplicate beneficiary numbers: ${duplicateBeneficiaries.join(', ')}`);
        }
        res.is.badRequest(400, `There's a problem with the uploaded information: ${errors.join(" AND ")}`);
        return;
    }

    // create user accounts for the beneficiaries if account doesn't already exists
    const [usersBatchUploadError] = await execSilentCallback(usersBatchUpload.execute, bindTo(usersBatchUpload))();
    usersBatchUploadError.ifError("Error setting up accounts for beneficiaries").thenStopExecution();

    // assign beneficiaries to ICEs
    const [userICEsBatchUploadError] = await execSilentCallback(userICEsBatchUpload.execute, bindTo(userICEsBatchUpload))();
    userICEsBatchUploadError.ifError("Error assigning ICEs to beneficiaries").thenStopExecution();

    // add beneficiaries to organization's profile
    const update = {
        $pullAll: { disabledBeneficiaries: userIDs },
        $addToSet: { beneficiaries: { $each: userIDs } }
    };
    const [updateError] = await execSilentCallback(orgCollection.findOneAndUpdate, bindTo(orgCollection))(query, update);
    updateError.ifError("Failed to update organization beneficiaries list on database").thenStopExecution();

    const result = {
        success: userIDs.length,
        failed: beneficiaries.length - userIDs.length,
        duplicates: duplicateBeneficiaries.join(', ')
    };
    res.is.ok(result);
}

function validateBeneficiary(beneficiary) {
    // beneficiary must be object
    if (typeof beneficiary !== "object") {
        return;
    }

    // check name and phone number
    if (!beneficiary.firstName || !beneficiary.phoneNumber) {
        return;
    }
    // check phone number
    if (beneficiary.phoneNumber.length !== 11 || beneficiary.phoneNumber.split().some(digit => isNaN(digit))) {
        return;
    }

    return beneficiary;
}

// check which ICE IDs actually exist in database
async function getValidBeneficiaryIceId(db, organizationId, beneficiary) {
    let query;
    if (beneficiary.iceGroup) {
        query = {
            "addedBy.accountId": organizationId,
            "addedBy.accountType": "organization",
            name: beneficiary.iceGroup
        };
    }
    else if (beneficiary.iceID) {
        query = {
            _id: beneficiary.iceID
        };
    }
    else {
        return;
    }

    const iceCollection = db.collection(constants.database.collections.ices);
    const options = { projection: { addedBy: false, name: false } };
    const [findError, foundICE] = await execSilentCallback(iceCollection.findOne, bindTo(iceCollection))(query, options);
    findError.ifError("failed to validate organization ICE ID").thenStopExecution();

    return foundICE ? foundICE._id : undefined;
}

async function updateBeneficiaryICE(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["beneficiaries"], res)) {
        return;
    }

    const beneficiaries = req.body.beneficiaries;
    if (!(beneficiaries instanceof Array)) {
        res.is.badRequest(400, `Expected an array, got '${typeof req.body.beneficiaries}'`);
        return;
    }
    if (beneficiaries.length === 0) {
        res.is.badRequest(400, "Provide at least one beneficiary information to update");
        return;
    }

    const db = require("../../config/db").getDatabase();

    const orgCollection = db.collection(constants.database.collections.organizations);
    const userICEsCollection = db.collection(constants.database.collections.user_ices);

    const query = { _id: req.organization.id };
    let options = { projection: { beneficiaries: true, disabledBeneficiaries: true } };
    const [getOrgError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    getOrgError.ifError("Failed to get current beneficiaries list from database").thenStopExecution();

    if (!org.beneficiaries) {
        org.beneficiaries = [];
    }
    if (!org.disabledBeneficiaries) {
        org.disabledBeneficiaries = [];
    }

    const userIDs = [];
    const userICEsBatchUpload = userICEsCollection.initializeUnorderedBulkOp();

    let hasInvalidDataCount = 0, hasInvalidBeneficiariesCount = 0;
    const invalidPhoneNumbers = [];

    for (const beneficiary of beneficiaries) {
        if (!beneficiary.phoneNumber || beneficiary.phoneNumber.length !== 11 || beneficiary.phoneNumber.split().some(digit => isNaN(digit))) {
            hasInvalidDataCount++;
            continue;
        }

        const iceID = await getValidBeneficiaryIceId(db, req.organization.id, beneficiary);
        if (!iceID) {
            hasInvalidBeneficiariesCount++;
            continue;
        }

        // if already processed or doesn't even exist in organization profile
        if (org.beneficiaries.indexOf(beneficiary.phoneNumber) < 0 &&
            org.disabledBeneficiaries.indexOf(beneficiary.phoneNumber) < 0) {
            invalidPhoneNumbers.push(beneficiary.phoneNumber);
            continue;
        }

        userIDs.push(beneficiary.phoneNumber);

        // upsert in case previously assigned ice was deleted and there's nothing to update
        userICEsBatchUpload.find({ _id: `${beneficiary.phoneNumber}_${req.organization.id}` }).upsert().updateOne({
            $setOnInsert: { userId: beneficiary.phoneNumber, status: "active" },
            $set: { iceID, assignDate: Date.now() }
        });
    }

    if (userIDs.length === 0) {
        const errors = [];
        if (hasInvalidDataCount > 0) {
            errors.push(`${hasInvalidDataCount} beneficiary info has missing or invalid phoneNumber`)
        }
        if (hasInvalidBeneficiariesCount > 0) {
            errors.push(`${hasInvalidBeneficiariesCount} beneficiaries have no valid ICE groups assigned to them`)
        }
        if (invalidPhoneNumbers.length > 0) {
            errors.push(`found invalid beneficiary numbers: ${invalidPhoneNumbers.join(', ')}`);
        }
        res.is.badRequest(400, `There's a problem with the uploaded information: ${errors.join(" AND ")}`);
        return;
    }

    // assign beneficiaries to ICEs
    const [userICEsBatchUploadError] = await execSilentCallback(userICEsBatchUpload.execute, bindTo(userICEsBatchUpload))();
    userICEsBatchUploadError.ifError("Error updating ICEs for beneficiaries").thenStopExecution();

    const result = {
        success: userIDs.length,
        failed: beneficiaries.length - userIDs.length,
        invalidBeneficiaries: invalidPhoneNumbers.join(', ')
    };
    res.is.ok(result);
}

async function enableOrDisableBeneficiary(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["phoneNumbers"], res)) {
        return;
    }
    let phoneNumbers = req.body.phoneNumbers;
    const totalCount = phoneNumbers.length;
    phoneNumbers = Array.from(new Set(phoneNumbers)); // remove duplicates
    const uniqueCount = phoneNumbers.length;

    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    // check if phoneNumbers are actually in organization's beneficiaries or disabledBeneficiaries list
    const options = { projection: {} };
    if (req.body.action === "enable") {
        options.projection = { disabledBeneficiaries: true };
    }
    else {
        options.projection = { beneficiaries: true };
    }
    const query = { _id: req.organization.id };
    const [getOrgError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    getOrgError.ifError("Failed to get beneficiaries list from database").thenStopExecution();

    // filter phone numbers that are actually enabled/disabled in this organization
    const checkList = org.beneficiaries || org.disabledBeneficiaries || [];
    phoneNumbers = phoneNumbers.filter(phoneNumber => checkList.indexOf(phoneNumber) >= 0);
    const validCount = phoneNumbers.length;

    if (validCount > 0) {
        const batchOperation = await getEnableOrDisableBeneficiariesBatchOp(req.body.action, orgCollection, req.organization.id,req.organization.name, phoneNumbers);
        const [error] = await execSilentCallback(batchOperation.execute, bindTo(batchOperation))();
        error.ifError(`Error ${req.body.action === "enable" ? 'enabling' : 'disabling'} beneficiaries`).thenStopExecution();
    }

    const result = {
        total: totalCount,
        duplicates: totalCount - uniqueCount,
        invalid: uniqueCount - validCount
    };
    if (req.body.action === "enable") {
        result.enabled = validCount;
    }
    else {
        result.disabled = validCount;
    }
    res.is.ok(result);
}

async function getEnableOrDisableBeneficiariesBatchOp(action, orgCollection, organizationId, organizationName, phoneNumbers) {
    const batchOperation = orgCollection.initializeUnorderedBulkOp();

    if (action === "enable") {
        for (const phoneNumber of phoneNumbers) {
            batchOperation.find({ _id: organizationId }).updateOne({
                $pull: { disabledBeneficiaries: phoneNumber.trim() },
                $addToSet: { beneficiaries: phoneNumber.trim() }
            });  
  
        }
    }
    else {
        for (const phoneNumber of phoneNumbers) {

            const message = `${organizationName} you have been disabled as a beneficiary`;
            await sendSMS(phoneNumber, message).catch(console.error);
            
            batchOperation.find({ _id: organizationId }).updateOne({
                $pull: { beneficiaries: phoneNumber.trim() },
                $set : {disabledBy : organizationId},
                $addToSet: { disabledBeneficiaries: phoneNumber.trim() }
            });
        }
    }

    return batchOperation;
}



module.exports = {
    getBeneficiaries,
    getAllBeneficiaries,
    addBeneficiaries,
    enableOrDisableBeneficiary,
    updateBeneficiaryICE
};