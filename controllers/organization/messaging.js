const { bindTo, execSilentCallback, silentPromise } = require("silent-promise");
const ObjectID = require('mongodb').ObjectID;
const notifications = require("../general/notifications");
const messaging = require("../general/messaging");
const transactions = require("../general/transactions");
const utils = require("../../utils");
const constants = require("../../utils/constants");

async function sendMessage(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["message"], res)) {
        return;
    }

    if (req.body.message.trim() === '') {
      res.is.badRequest(400, "Message cannot be empty");
      return;
    }

    const sendSMS = !req.body.ignoreSMS;
    const sendInApp = !req.body.ignoreApp;
    if (!sendSMS && !sendInApp) {
        res.is.badRequest(400, "Must send message either via SMS or to App");
        return;
    }

    // validate
    if (req.body.ices && !(req.body.ices instanceof Array)) {
        res.is.badRequest(400, `ICEs should be an array. Got ${typeof req.body.ices}`);
        return;
    }
    if (req.body.ices && req.body.ices.length === 0) {
      res.is.badRequest(400, `Provide at least 1 ICE if sending to users in ICE group`);
      return;
    }
    if (req.body.location) {
        if (!req.body.location.latitude || !req.body.location.longitude || !req.body.location.radius) {
            res.is.badRequest(400, "location should be an object containing latitude, longitude and radius");
            return;
        }
    }

    let userIds;
    if (req.body.ices) {
        // get user ids from user-ices link table
        userIds = await getUsersForICEs(req.body.ices, req.organization.id);
    }
    else {
        userIds = await getEnabledUsersInOrganization(req.organization.id);
    }

    if (userIds.length === 0) {
        res.is.ok({ recipientsCount: 0 });
        return;
    }

    const message = req.body.message;
    const subject = req.body.subject || `New message from ${req.organization.name}`;

    const smsUnits = Math.ceil(message.length / 160);
    const costPerSMS = 4;

    // estimate sms bill
    const estimatedSMSCost = smsUnits * costPerSMS * userIds.length;
    if (sendSMS === true && req.organization.walletBalance < estimatedSMSCost) {
        res.is.badRequest(400, `Wallet balance not enough to cover SMS charges. Estimated SMS cost: ${estimatedSMSCost} naira, 
        balance: ${req.organization.walletBalance} naira`);
        return;
    }

    const query = { _id: { $in: userIds } };
    if (req.body.location) {
        query.location = {
            $near: {
                $geometry: { type: "Point", coordinates: [+req.body.location.longitude, +req.body.location.latitude] },
                $maxDistance: +req.body.location.radius
            }
        }
    }

    const db = require("../../config/db").getDatabase();
    const usersCollection = db.collection(constants.database.collections.users);
    const options = { projection: { notificationIds: true } };

    const [findError, cursor] = await execSilentCallback(usersCollection.find, bindTo(usersCollection))(query, options);
    findError.ifError("failed to get beneficiaries info from database").thenStopExecution();

    let smsCount = 0, totalMatches = 0;
    while (await cursor.hasNext()) {
        const userInfo = await cursor.next();
        totalMatches++;

        if (sendInApp === true) {
            const userAccount = {
                id: userInfo._id,
                collection: constants.database.collections.users
            };
            await silentPromise(notifications.sendNotificationToApp(userInfo.notificationIds, subject, message, userAccount))
        }
        if (sendSMS === true) {
            await silentPromise(messaging.sendSMS(userInfo._id, message, req.body.smsSenderName));
            smsCount++;
        }
    }

    if (smsCount > 0) {
        // bill organization for SMS units
        const bill = smsCount * smsUnits * costPerSMS;
        await silentPromise(transactions.debitAccount("organization", req.organization.id, new ObjectID().toHexString(), Date.now(), bill,
            "SMS Charges", `SMS to ${smsCount} users`));
    }

    res.is.ok({ recipientsCount: totalMatches });
}

async function getUsersForICEs(ices, organizationId) {
    const db = require("../../config/db").getDatabase();
    const userICEsCollection = db.collection(constants.database.collections.user_ices);

    const query = {
        iceID: { $in: ices }
    };
    const options = { projection: { userId: true } };

    const [findError, cursor] = await execSilentCallback(userICEsCollection.find, bindTo(userICEsCollection))(query, options);
    findError.ifError("failed to get organization ICEs from database").thenStopExecution();

    const disabledUsers = await getDisabledUsersInOrganization(organizationId);

    const userIds = new Set();
    while (await cursor.hasNext()) {
        const linkInfo = await cursor.next();
        if (disabledUsers.indexOf(linkInfo.userId) < 0) {
          userIds.add(linkInfo.userId);
        }
    }

    return Array.from(userIds);
}

async function getEnabledUsersInOrganization(organizationId) {
    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    const query = { _id: organizationId };
    const projection = { projection: { beneficiaries: true } };

    const [updateError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, projection);
    updateError.ifError("Failed to get beneficiaries from database").thenStopExecution();

    const beneficiaryIds = [];
    if (org.beneficiaries) {
        beneficiaryIds.push(...org.beneficiaries);
    }

    return beneficiaryIds;
}

async function getDisabledUsersInOrganization(organizationId) {
  const db = require("../../config/db").getDatabase();
  const orgCollection = db.collection(constants.database.collections.organizations);

  const query = { _id: organizationId };
  const projection = { projection: { disabledBeneficiaries: true } };

  const [updateError, org] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, projection);
  updateError.ifError("Failed to get disabled beneficiaries from database").thenStopExecution();

  const beneficiaryIds = [];
  if (org.disabledBeneficiaries) {
    beneficiaryIds.push(...org.disabledBeneficiaries);
  }

  return beneficiaryIds;
}

module.exports = {
    sendMessage
};