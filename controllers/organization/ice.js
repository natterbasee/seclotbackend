const {bindTo, execSilentCallback} = require('silent-promise');
const ObjectID = require('mongodb').ObjectID;
const utils = require('../../utils');
const constants = require('../../utils/constants');
const logger = require('../../utils/logger');
const {sendSMS} = require('../../controllers/general/messaging');

async function getICEs(req, res) {
	const query = {
		'addedBy.accountId': req.organization.id,
		'addedBy.accountType': 'organization'
	};
	const options = {projection: {addedBy: false}};

	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	const [findError, cursor] = await execSilentCallback(
		iceCollection.find,
		bindTo(iceCollection)
	)(query, options);
	findError
		.ifError('failed to get organization ICEs from database')
		.thenStopExecution();

	let ices = await cursor.toArray();
	ices = ices.map(ice => {
		ice.id = ice._id;
		delete ice._id;
		return ice;
	});

	res.is.ok(ices);
}

async function addICE(req, res) {
	if (utils.parametersAreIncomplete(req.body, ['name', 'phoneNumbers'], res)) {
		return;
	}

	// check phone numbers
	const phoneNumbers = req.body.phoneNumbers
		.split(',')
		.map(phoneNumber => phoneNumber.trim());
	if (
		phoneNumbers.some(
			phoneNumber =>
				phoneNumber.length !== 11 ||
        phoneNumber.split().some(digit => isNaN(digit))
		)
	) {
		res.is.badRequest(400, 'Each phone number in ICE group must be 11 digits');
		return;
	}

	const organizationId = req.organization.id;
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);

	// check for any other ICE added by this organization with the same name or phone number
	const duplicateQuery = {
		'addedBy.accountId': organizationId,
		'addedBy.accountType': 'organization',
		name: req.body.name
	};
	const options = {projection: {name: true}};
	const [findError, ice] = await execSilentCallback(
		iceCollection.findOne,
		bindTo(iceCollection)
	)(duplicateQuery, options);
	findError
		.ifError('Error checking registration info for duplicated info')
		.thenStopExecution();

	if (ice) {
		res.is.badRequest(400, 'Another ICE exists with the same name');
		return;
	}

	const newICE = {
		_id: new ObjectID().toHexString(),
		name: req.body.name,
		phoneNumber: phoneNumbers.join(','),
		addedBy: {
			accountId: organizationId,
			accountType: 'organization'
		},
		dateAdded: Date.now()
	};

	const message = 'You have just been added as an ICE on Seclot.';
	await sendSMS(phoneNumbers, message).catch(console.error);

	const [saveError] = await execSilentCallback(
		iceCollection.insertOne,
		bindTo(iceCollection)
	)(newICE);
	saveError.ifError('Failed to add ICE to organization').thenStopExecution();

	await getICEs(req, res);
}

async function updateICE(req, res) {
	if (
		utils.hasNoneOfTheRequiredParameters(
			req.body,
			['name', 'phoneNumbers'],
			res
		)
	) {
		return;
	}

	let phoneNumbers;
	if (req.body.phoneNumbers) {
		// check phone numbers
		phoneNumbers = req.body.phoneNumbers
			.split(',')
			.map(phoneNumber => phoneNumber.trim());
		if (
			phoneNumbers.some(
				phoneNumber =>
					phoneNumber.length !== 11 ||
          phoneNumber.split().some(digit => isNaN(digit))
			)
		) {
			res.is.badRequest(
				400,
				'Each phone number in ICE group must be 11 digits'
			);
			return;
		}
	}

	const organizationId = req.organization.id;
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);

	if (req.body.name) {
		// check for any other ICE added by this organization with the same name that is NOT this ICE to be updated
		const duplicateQuery = {
			_id: {$ne: req.params.iceId},
			'addedBy.accountId': organizationId,
			'addedBy.accountType': 'organization',
			name: req.body.name
		};

		const options = {projection: {name: true}};
		const [findError, ice] = await execSilentCallback(
			iceCollection.findOne,
			bindTo(iceCollection)
		)(duplicateQuery, options);
		findError
			.ifError('Error checking update info for duplicated info')
			.thenStopExecution();

		if (ice) {
			res.is.badRequest(400, 'Another ICE exists with the same name');
			return;
		}
	}

	const update = {};
	if (req.body.name) {
		update.name = req.body.name;
	}
	if (phoneNumbers) {
		update.phoneNumber = phoneNumbers.join(',');
	}

	const query = {_id: req.params.iceId};
	const [saveError] = await execSilentCallback(
		iceCollection.updateOne,
		bindTo(iceCollection)
	)(query, {$set: update});
	saveError.ifError('Failed to update ICE information').thenStopExecution();

	await getICEs(req, res);
}

async function removeICE(req, res) {
	const db = require('../../config/db').getDatabase();
	let deleteError;

	let query = {
		_id: req.params.iceId,
		'addedBy.accountId': req.organization.id,
		'addedBy.accountType': 'organization'
	};
	const iceCollection = db.collection(constants.database.collections.ices);
	[deleteError] = await execSilentCallback(
		iceCollection.deleteOne,
		bindTo(iceCollection)
	)(query);
	deleteError
		.ifError('Failed to remove ICE from organization')
		.thenStopExecution();

	// also delete from user-ice link table
	query = {iceID: req.params.iceId};
	const userICEsCollection = db.collection(
		constants.database.collections.user_ices
	);
	const [findError, cursor] = await execSilentCallback(
		userICEsCollection.find,
		bindTo(userICEsCollection)
	)(query);

	console.log(findError);

	// const recipients = cursor.phoneNumber.map(phoneNumber => `234${phoneNumber.substr(1)}`).join(",");

	const message = 'You have been deleted as an ice!';
	await sendSMS(cursor.phoneNumber, message).catch(console.error);

	[deleteError] = await execSilentCallback(
		userICEsCollection.deleteMany,
		bindTo(userICEsCollection)
	)(query);
	deleteError
		.ifError('Failed to remove ICE from beneficiaries profile')
		.thenDo((error, errorMessage) => {
			logger.error(
				{detail: error, path: req.path, method: req.method},
				errorMessage
			);
		});

	await getICEs(req, res);
}

async function pauseIce(req, res) {
	if (utils.parametersAreIncomplete(req.params, ['iceId'], res)) {
		return;
	}
	const query = {_id: req.params.iceId};
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	const [saveError, opsResult] = await execSilentCallback(
		iceCollection.updateOne,
		bindTo(iceCollection)
	)(query, {$set: {paused: true}});
	saveError.ifError('Failed to update ICE information').thenStopExecution();

	if (opsResult.matchedCount === 0) {
		res.is.badRequest(400, 'Invalid ICE ID');
		return;
	}
	res.is.ok({message: 'Ice paused successfully from alerts'});
}

async function unPauseIce(req, res) {
	if (utils.parametersAreIncomplete(req.params, ['iceId'], res)) {
		return;
	}
	const db = require('../../config/db').getDatabase();
	const iceCollection = db.collection(constants.database.collections.ices);
	const query = {_id: req.params.iceId};
	const [saveError, opsResult] = await execSilentCallback(
		iceCollection.updateOne,
		bindTo(iceCollection)
	)(query, {$set: {paused: false}});
	saveError.ifError('Failed to update ICE information').thenStopExecution();

	if (opsResult.matchedCount === 0) {
		res.is.badRequest(400, 'Invalid ICE ID');
		return;
	}
	res.is.ok({message: 'Ice unpaused successfully from alerts'});
}

module.exports = {
	getICEs,
	addICE,
	unPauseIce,
	pauseIce,
	updateICE,
	removeICE
};
