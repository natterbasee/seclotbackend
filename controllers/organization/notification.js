const {bindTo, execSilentCallback} = require('silent-promise')
const utils = require('../../utils')
const constants = require('../../utils/constants')
const notifications = require('../general/notifications')

async function registerFCMID(req, res) {
  if (utils.parametersAreIncomplete(req.body, ['notificationId'], res)) {
    return
  }
  if (typeof req.body.notificationId !== 'string') {
    res.is.badRequest(400, 'Invalid notificationId')
    return
  }

  const query = {_id: req.organization.id}
  const update = {
    $addToSet: {notificationIds: req.body.notificationId},
  }

  const db = require('../../config/db').getDatabase()
  const orgCollection = db.collection(
    constants.database.collections.organizations
  )
  const [updateError] = await execSilentCallback(
    orgCollection.updateOne,
    bindTo(orgCollection)
  )(query, update)
  updateError
    .ifError("Failed to add notificationId to organization's profile")
    .thenStopExecution()

  res.is.ok({message: 'Notification id added to organization account'})
}

async function sendNotificationToApp(req, res) {
  if (utils.parametersAreIncomplete(req.body, ['subject', 'message'], res)) {
    return
  }
  if (
    typeof req.body.subject !== 'string' ||
    typeof req.body.message !== 'string'
  ) {
    res.is.badRequest(400, 'Invalid subject or message')
    return
  }

  const thisAccount = {
    id: req.organization.id,
    collection: constants.database.collections.organizations,
  }
  const response = await notifications.sendNotificationToApp(
    req.organization.notificationIds,
    req.body.subject,
    req.body.message,
    thisAccount
  )
  res.is.ok(response ? response : {sentToDevices: 0})
}

module.exports = {
  registerFCMID,
  sendNotificationToApp,
}
