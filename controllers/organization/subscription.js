const { bindTo, execSilentCallback } = require("silent-promise");
const utils = require("../../utils");
const constants = require("../../utils/constants");
const settings = require("../../controllers/general/settings");

async function updateSubscriptionPlan(req, res) {
    if (utils.parametersAreIncomplete(req.body, ["plan"], res)) {
        return;
    }
    if (typeof req.body.plan !== "string") {
        res.is.badRequest(400, `Expected plan to be 'string', got ${typeof req.body.plan}`);
        return;
    }

    const plans = await settings.getSubscriptionPlans();
    let selectedPlan, selectedPlanName;
    for (const planName of Object.keys(plans)) {
        if (planName.toLowerCase() === req.body.plan.toLowerCase()) {
            selectedPlan = plans[planName];
            selectedPlanName = planName;
            break;
        }
    }
    if (!selectedPlan) {
        res.is.badRequest(400, "Invalid plan selected");
        return;
    }

    const db = require("../../config/db").getDatabase();
    const orgCollection = db.collection(constants.database.collections.organizations);

    const query = { _id: req.organization.id };
    const options = { projection: { plan: true, walletBalance: true, beneficiaries: true, disabledBeneficiaries: true } };
    const [error, organization] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
    error.ifError("Failed to update organization's subscription plan").thenStopExecution();

    if (organization.plan && organization.plan.toLowerCase() === selectedPlanName.toLowerCase()) {
        res.is.badRequest(400, "Already subscribed to this plan");
        return;
    }

    const totalBillableUsersInOrg = (organization.beneficiaries || []).length + (organization.disabledBeneficiaries || []).length;
    const nextBill = totalBillableUsersInOrg * selectedPlan.amount;
    if (!organization.walletBalance || organization.walletBalance < nextBill) {
        res.is.badRequest(400, `You do not have enough funds to subscribe to this plan. You need to top up your wallet with at least 
        ${nextBill - (organization.walletBalance || 0)} Naira to cover payment for your ${totalBillableUsersInOrg} beneficiaries`);
        return;
    }

    // save plan to profile, set status as active and mark next billing date as now
    const orgUpdate = {
        plan: selectedPlanName,
        subscriptionStatus: "active",
        nextBillDate: Date.now()
    };

    const [updateError] = await execSilentCallback(orgCollection.updateOne, bindTo(orgCollection))(query, { $set: orgUpdate });
    updateError.ifError("Failed to update organization's subscription plan").thenStopExecution();

    return res.is.ok(orgUpdate);
}

module.exports = {
    updateSubscriptionPlan
};