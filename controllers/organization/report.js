const { bindTo, execSilentCallback } = require("silent-promise");
const createPDFDoc = require('../general/pdf-printer');
const utils = require("../../utils");
const constants = require("../../utils/constants");
const { getAllBeneficiaries } = require("./beneficiary");

async function iceReport(req, res) {
    const db = require("../../config/db").getDatabase();
    const iceCollection = db.collection(constants.database.collections.ices);
    const userICEsCollection = db.collection(constants.database.collections.user_ices);
    const distressCollection = db.collection(constants.database.collections.distressCalls);

    // get ices (details, number of beneficiaries assigned, number of distress calls received
    const query = {
        "addedBy.accountId": req.organization.id,
        "addedBy.accountType": "organization"
    };
    const options = { projection: { addedBy: false } };
    const [findError, icesCursor] = await execSilentCallback(iceCollection.find, bindTo(iceCollection))(query, options);
    findError.ifError("failed to get organization ICEs from database").thenStopExecution();

    let error, count;
    const ices = [];
    while (await icesCursor.hasNext()) {
        const ice = await icesCursor.next();

        ice.id = ice._id;
        delete ice._id;

        // get number of beneficiaries assigned to each ICE from the ice link table
        [error, count] = await execSilentCallback(userICEsCollection.countDocuments, bindTo(userICEsCollection))({ iceID: ice.id });
        error.ifError("Error getting beneficiaries count for ICE").thenStopExecution();
        ice.beneficiaries = count;

        // get number of distress calls submitted to each ICE from the distress call table
        [error, count] = await execSilentCallback(distressCollection.countDocuments, bindTo(distressCollection))({ icesNotified: ice.id });
        error.ifError("Error getting distress call count for ICE").thenStopExecution();
        ice.distressCalls = count;

        ices.push([
            ices.length + 1,
            ice.name,
            ice.phoneNumber.split(",").join(", "),
            ice.beneficiaries,
            ice.distressCalls
        ]);
    }

    const tableRowHeader = [
      { text: 'S/N', style: 'tableHeader' },
      { text: 'Name', style: 'tableHeader' },
      { text: 'Phone Number(s)', style: 'tableHeader' },
      { text: 'Beneficiaries', style: 'tableHeader' },
      { text: 'Distress Calls', style: 'tableHeader' }
    ];
    const tableColumnWidths = [ 30, 'auto', '*', 'auto', 'auto' ];
    const doc = createPDFDoc('ICE Report', `${req.organization.name} - ${new Date().toDateString()}`, tableRowHeader, ices, tableColumnWidths);
    res.is.pdf(doc);
}

async function beneficiaryReport(req, res) {
    const db = require("../../config/db").getDatabase();
    const distressCollection = db.collection(constants.database.collections.distressCalls);
    const organizationId = req.organization.id;

    // get all beneficiaries and append distress call count to each
    const beneficiaries = [];

    for (const beneficiary of await getAllBeneficiaries(organizationId)) {
        // get number of distress calls submitted to organization by beneficiary
        const query = {
            userId: beneficiary.phoneNumber,
            organizationsNotified: organizationId
        };
        const [error, count] = await execSilentCallback(distressCollection.countDocuments, bindTo(distressCollection))(query);
        error.ifError("Error getting distress call count for beneficiary").thenStopExecution();
        beneficiary.distressCalls = count;

        beneficiaries.push([
            beneficiaries.length + 1,
            `${beneficiary.firstName} ${beneficiary.lastName}`,
            beneficiary.phoneNumber,
            beneficiary.ice ? beneficiary.ice.name : 'Not assigned',
            beneficiary.distressCalls,
            beneficiary.organizationDisabled ? 'Disabled' : 'Active'
        ]);
    }

    const tableRowHeader = [
      { text: 'S/N', style: 'tableHeader' },
      { text: 'Name', style: 'tableHeader' },
      { text: 'Phone Number', style: 'tableHeader' },
      { text: 'ICE Group', style: 'tableHeader' },
      { text: 'Distress Calls', style: 'tableHeader' },
      { text: 'Status', style: 'tableHeader' }
    ];
    const tableColumnWidths = [ 30, '*', 'auto', 'auto', 'auto', 'auto' ];
    const doc = createPDFDoc('Beneficiaries Report', `${req.organization.name} - ${new Date().toDateString()}`, tableRowHeader,
        beneficiaries, tableColumnWidths);

    res.is.pdf(doc);
}

async function walletReport(req, res) {
  const txTypes = ["debit", "credit"];
  if (req.query.filter && txTypes.indexOf(req.query.filter.toLowerCase()) < 0) {
    res.is.badRequest(400, `filter must be one of ${txTypes.join(", ")}`);
    return;
  }

  const transactions = await getTransactions(req.organization.id, req.query.filter);
  res.is.ok(transactions);
}

async function walletReportPDF(req, res) {
  let transactions = await getTransactions(req.organization.id);
  transactions = transactions.map((tx, index) => {
    return [
        index + 1,
        new Date(tx.paymentDate).toDateString(),
        tx.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        tx.txType,
        tx.shortDescription
    ]
  });

  const tableRowHeader = [
    { text: 'S/N', style: 'tableHeader' },
    { text: 'Date', style: 'tableHeader' },
    { text: 'Amount (Naira)', style: 'tableHeader' },
    { text: 'Type', style: 'tableHeader' },
    { text: 'Description', style: 'tableHeader' }
  ];
  const tableColumnWidths = [ 30, 'auto', 'auto', 'auto', '*' ];
  const doc = createPDFDoc('Wallet Report', `${req.organization.name} - ${new Date().toDateString()}`, tableRowHeader,
      transactions, tableColumnWidths);

  res.is.pdf(doc);
}

async function getTransactions(organizationId, filter) {
  const query = {
    entity: {
      id: organizationId,
      type: "organization"
    }
  };
  if (filter) {
    query.txType = filter.toLowerCase()
  }
  const options = { projection: { _id: false, entity: false, processedDate: false } };

  const db = require("../../config/db").getDatabase();
  const txCollection = db.collection(constants.database.collections.transactions);
  const [error, transactionsCursor] = await execSilentCallback(txCollection.find, bindTo(txCollection))(query, options);
  error.ifError("Error retrieving organization's transaction records").thenStopExecution();

  return transactionsCursor.toArray();
}

// dashboard displays number of ices, beneficiaries and array of distress calls
async function dashboardReport(req, res) {
  const db = require("../../config/db").getDatabase();
  const distressCollection = db.collection(constants.database.collections.distressCalls);

  // get ices count
  let query = {
    "addedBy.accountId": req.organization.id,
    "addedBy.accountType": "organization"
  };
  const iceCollection = db.collection(constants.database.collections.ices);
  const [countError, icesCount] = await execSilentCallback(iceCollection.countDocuments, bindTo(iceCollection))(query);
  countError.ifError("failed to get organization ICEs count from database").thenStopExecution();

  // get beneficiaries count
  query = { _id: req.organization.id };
  const options = { projection: { beneficiaries: true, disabledBeneficiaries: true } };
  const orgCollection = db.collection(constants.database.collections.organizations);
  const [findError, organization] = await execSilentCallback(orgCollection.findOne, bindTo(orgCollection))(query, options);
  findError.ifError("failed to get beneficiaries count from database").thenStopExecution();
  const beneficiariesCount = (organization.beneficiaries || []).length + (organization.disabledBeneficiaries || []).length

  // get distress calls
  query = {
    organizationsNotified: req.organization.id
  };
  const [error, cursor] = await execSilentCallback(distressCollection.find, bindTo(distressCollection))(query);
  error.ifError("Error getting distress call history").thenStopExecution();
  const history = await cursor.toArray();

  // get names of each user
  const userIds = new Set();
  for (const call of history) {
    userIds.add(call.userId);
  }

  const usersCollection = db.collection(constants.database.collections.users);
  const userQuery = { _id: { $in: Array.from(userIds) } };
  options.projection = { firstName: true, lastName: true, seclotId: true, location: true };
  const [getUsersError, usersCursor] = await execSilentCallback(usersCollection.find, bindTo(usersCollection))(userQuery, options);
  getUsersError.ifError("Error getting user info for distress calls").thenStopExecution();

  const usersArr = await usersCursor.toArray();
  const users = {};
  for (const user of usersArr) {
    user.phoneNumber = user._id;
    delete user._id;
    users[user.phoneNumber] = user;
  }

  const distressCalls = history.map(call => {
    return {
      callDate: call.callDate,
      status: call.status,
      beneficiary: utils.getFormattedProfile(users[call.userId])
    };
  });

  res.is.ok({ icesCount, beneficiariesCount, distressCalls });
}

module.exports = {
  iceReport,
  beneficiaryReport,
  walletReport,
  walletReportPDF,
  dashboardReport
};