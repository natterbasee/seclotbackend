const FirebaseAuth = require('firebaseauth')
const {bindTo, execSilentCallback} = require('silent-promise')
const {sendNewSignupNotificationToAdmin} = require('../admin/notification')
const utils = require('../../utils')
const constants = require('../../utils/constants')
const {getNextId} = require('../general/auth')
const Mail = require('../../mail/mail')

async function login(req, res) {
  if (utils.parametersAreIncomplete(req.body, ['email', 'password'], res)) {
    return
  }
  req.body.email = req.body.email.toLowerCase()

  const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY)
  utils.bindMethodsToObject(firebaseAuth, FirebaseAuth)
  const [authError, authResult] = await execSilentCallback(
    firebaseAuth.signInWithEmail,
    req.body.email,
    req.body.password
  )
  if (authError.getError()) {
    res.is.badRequest(401, authError.getError().message)
    return
  }

  const findById = {_id: authResult.user.id}
  const options = {
    projection: {beneficiaries: false, disabledBeneficiaries: false},
  }

  const db = require('../../config/db').getDatabase()
  const orgCollection = db.collection(
    constants.database.collections.organizations
  )
  const [findError, organization] = await execSilentCallback(
    orgCollection.findOne,
    bindTo(orgCollection)
  )(findById, options)
  findError
    .ifError('failed to get organization info from database')
    .thenStopExecution()

  if (!organization) {
    return res.is.badRequest(403, 'This is NOT an Organization account')
  } else {
    if (!organization.confirmed) {
      const token = generateID(100)
      const [updateError] = await execSilentCallback(
        orgCollection.updateOne,
        bindTo(orgCollection)
      )(findById, {$set: {confirmToken: token}})
      await Mail.send({
        subject: 'Account Verification',
        to: req.body.email,
        filename: 'account-confirm',
        name: organization.name,
        confirmLink: `${process.env.FRONTEND_SERVER}/confirm-account?token=${token}`,
      })
      return res.is.badRequest(
        403,
        'This email account has not been confirmed yet. Please check your email'
      )
    }
    if (!organization.seclotId) {
      const newSeclotId = await getNextId()
      const [updateError] = await execSilentCallback(
        orgCollection.updateOne,
        bindTo(orgCollection)
      )(findById, {$set: {seclotId: newSeclotId}})
      updateError
        .ifError('failed to update organization seclot id')
        .thenStopExecution()
    }

    return res.is.ok({
      profile: utils.getFormattedProfile(organization, req),
      refreshToken: authResult.refreshToken,
      token: authResult.token,
      tokenExpiry: authResult.expiryMilliseconds,
    })
  }
}

async function confirm(req, res) {
  if (utils.parametersAreIncomplete(req.params, ['token'], res)) {
    return
  }

  const findByToken = {confirmToken: req.params.token}

  const db = require('../../config/db').getDatabase()
  const orgCollection = db.collection(
    constants.database.collections.organizations
  )
  const [findError, organization] = await execSilentCallback(
    orgCollection.findOne,
    bindTo(orgCollection)
  )(findByToken)
  findError.ifError('Invalid token provided').thenStopExecution()

  if (!organization) {
    res.is.badRequest(403, 'This token is Invalid')
  } else {
    const [updateError] = await execSilentCallback(
      orgCollection.updateOne,
      bindTo(orgCollection)
    )(findByToken, {$set: {confirmed: true}})
    updateError
      .ifError('failed to update organization seclot id')
      .thenStopExecution()
    res.is.ok({
      message: 'Account confirmation successfull',
    })
  }
}

async function register(req, res) {
  if (
    utils.parametersAreIncomplete(
      req.body,
      ['name', 'email', 'password', 'phoneNumber', 'location'],
      res
    )
  ) {
    return
  }
  req.body.email = req.body.email.toLowerCase()

  if (req.body.password.length < 6) {
    res.is.badRequest(400, 'Password must be 6 or more characters')
    return
  }
  if (utils.passwordIsNotValid(req.body.password)) {
    res.is.badRequest(400, utils.passwordErrorMessage)
    return
  }

  if (req.body.phoneNumber.length !== 11) {
    res.is.badRequest(400, 'Phone number should be 11 digits')
    return
  }
  if (req.body.phoneNumber.split().some((digit) => isNaN(digit))) {
    res.is.badRequest(400, 'Phone number is invalid')
    return
  }

  const db = require('../../config/db').getDatabase()
  const orgCollection = db.collection(
    constants.database.collections.organizations
  )
  const duplicateFields = await getDuplicatedFields(
    ['name', 'email', 'phoneNumber'],
    req.body,
    orgCollection
  )

  if (duplicateFields.length > 0) {
    res.is.badRequest(
      400,
      `An organization exists with the same ${duplicateFields.join(', ')}`
    )
    return
  }

  // register on firebase, then save to database
  const firebaseAuth = new FirebaseAuth(process.env.FIREBASE_API_KEY)
  utils.bindMethodsToObject(firebaseAuth, FirebaseAuth)
  const [authError, authResult] = await execSilentCallback(
    firebaseAuth.registerWithEmail,
    req.body.email,
    req.body.password,
    req.body.name
  )
  if (authError.getError()) {
    res.is.badRequest(401, authError.getError().message)
    return
  }
  const token = generateID(100)
  const newSeclotId = await getNextId()
  const newOrgInfo = {
    _id: authResult.user.id,
    name: req.body.name,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    location: req.body.location,
    seclotId: newSeclotId,
    confirmToken: token,
    accountCreationDate: Date.now(),
  }
  const [saveOrgInfoError] = await execSilentCallback(
    orgCollection.insertOne,
    bindTo(orgCollection)
  )(newOrgInfo)
  saveOrgInfoError
    .ifError('Failed to save new organization info to database')
    .thenStopExecution()

  // send confimation email
  await Mail.send({
    subject: 'Account Verification',
    to: req.body.email,
    filename: 'account-confirm',
    name: req.body.name,
    confirmLink: `${process.env.FRONTEND_SERVER}/confirm-account?token=${token}`,
  })
  sendNewSignupNotificationToAdmin(
    newOrgInfo.name,
    newOrgInfo.phoneNumber,
    'organization'
  )

  res.is.ok({
    message:
      'Account Created successfully. Please confirm your email address to continue',
  })
}

const generateID = function async(len = 10) {
  var pwdChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  var randId = Array(len)
    .fill(pwdChars)
    .map(function (x) {
      return x[Math.floor(Math.random() * x.length)]
    })
    .join('')
  return randId
}

async function getDuplicatedFields(
  fieldsToCheckForDuplicates,
  data,
  orgCollection
) {
  const checkFieldsForDuplicatesArray = []
  const options = {projection: {}}

  for (const field of fieldsToCheckForDuplicates) {
    options.projection[field] = true

    const fieldCheck = {}
    fieldCheck[field] = data[field]
    checkFieldsForDuplicatesArray.push(fieldCheck)
  }

  const duplicateQuery = {$or: checkFieldsForDuplicatesArray}
  const [findError, organization] = await execSilentCallback(
    orgCollection.findOne,
    bindTo(orgCollection)
  )(duplicateQuery, options)
  findError
    .ifError('Error checking registration info for duplicated info')
    .thenStopExecution()

  const duplicateFields = []

  if (organization) {
    for (const field of fieldsToCheckForDuplicates) {
      if (organization[field] === data[field]) {
        duplicateFields.push(field)
      }
    }
  }

  return duplicateFields
}

module.exports = {
  login,
  register,
  confirm,
}
