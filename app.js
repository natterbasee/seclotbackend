// load env variable first!
require("dotenv").config();
const { setupDatabase } = require("./config/db");
const logger = require("./utils/logger");

const express = require("express");
const setupServer = require("./config/setup");
const routes = require("./routes");
const { handleError } = require("./utils/promise-sandbox");

const { processOrganizationSubscriptionsRecurrently } = require("./controllers/queues/org-subscription-queue");
const { processUserSubscriptionsRecurrently } = require("./controllers/queues/user-subscription-queue");
const { runOrganizationReferralCreditCronJob, creditOrganizationReferrals } = require("./controllers/queues/referral-cron-job");

const app = express();
setupServer(app);

app.use("/admin", routes.adminRoutes);
app.use("/org", routes.organizationRoutes);
app.use("/user", routes.userRoutes);
app.use(routes.otherRoutes);

// temporary endpoint
app.get("/test-org-referral-bonuses", (req, res) => {
    creditOrganizationReferrals()
        .then(res.is.ok)
        .catch(error => {
            let detail;
            if (error.error) {
                detail = error.error;
            }
            else {
                detail = error.stack;
            }

            logger.error({ detail, path: "test-org-referral-bonuses" }, error.message);
            res.is.serverError({ detail, error: error.message });
        });
});

app.use((err, req, res, next) => {
    handleError(err, req, res);
});

setupDatabase()
    .then(() => {
        processOrganizationSubscriptionsRecurrently();
        processUserSubscriptionsRecurrently();
        runOrganizationReferralCreditCronJob();

        app.listen(process.env.PORT, () => {
            console.log(`Seclot API running on port ${process.env.PORT}`);
        });
    })
    .catch(error => {
        logger.error({ detail: error.stack || error, path: "app.js", method: "setupDatabase" }, "Failed to start server. Error setting up database");
    });