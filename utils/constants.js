module.exports = {
    database: {
        name: "seclot",
        collections: {
            admins: "admins",
            organizations: "organizations",
            ices: "ices",
            users: "users",
            user_ices: "user_ices",
            transactions: "transactions",
            distressCalls: "distress_calls",
            accountId: "account_id"
        }
    },
    otpRedisKey: "phone_otp_codes"
};