const logger = require("./logger");

function bindMethodsToObject(object, objClass) {
    Object.getOwnPropertyNames(objClass.prototype)
        .forEach(method => {
            if (method.startsWith('component')) {
                return;
            }

            object[method] = object[method].bind(object);
        });
}

function parametersAreIncomplete(data, requiredFields, res) {
    if (!data) {
        return requiredFields;
    }

    const missingParameters = [];
    for (const field of requiredFields) {
        if (data[field] === undefined) {
            missingParameters.push(field);
        }
    }

    if (missingParameters.length > 0) {
        res.is.badRequest(400, `Missing parameter(s): ${missingParameters.join(", ")}`);
    }

    return missingParameters.length > 0;
}

function hasNoneOfTheRequiredParameters(data, requiredFields, res) {
    if (!data) {
        return requiredFields;
    }

    const missingParameters = [];
    for (const field of requiredFields) {
        if (data[field] === undefined) {
            missingParameters.push(field);
        }
    }

    if (missingParameters.length === requiredFields.length) {
        res.is.badRequest(400, `Provide at least one of the following parameter(s): ${missingParameters.join(", ")}`);
        return true;
    }

    return false;
}

function getFormattedProfile(info, req) {
    if (info.picture && !info.picture.startsWith("http")) {
        info.picture = `http://${req.headers.host}/image/${info.picture}`;
    }

    delete info.id;
    delete info._id;
    delete info.notificationIds;

    // for users
    delete info.pinHash;
    if (info.location && info.location.coordinates) {
        info.lastKnownLocation = {
            longitude: info.location.coordinates[0],
            latitude: info.location.coordinates[1]
        };
        delete info.location;
    }

    return info;
}

function passwordIsNotValid(password) {
    // password >= 6 characters, letters, numbers and special characters
    return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/.test(password) === false;
}

function logError(error) {
    let detail;
    if (error.error) {
        detail = error.error;
    }
    else {
        detail = error.stack;
    }

    logger.error({ detail }, error.message);
}

const passwordErrorMessage = "Password must contain at least 1 letter, 1 number and 1 special character";

module.exports = {
    bindMethodsToObject,
    parametersAreIncomplete,
    hasNoneOfTheRequiredParameters,
    getFormattedProfile,
    passwordIsNotValid,
    logError,
    passwordErrorMessage
};