const { MongoClient } = require("mongodb");
const { silentCallback } = require("silent-promise");
const constants = require("../utils/constants");

let databaseClient, defaultDatabase;

async function setupDatabase() {
    const [error, client] = await silentCallback(MongoClient.connect)(process.env.MONGODB_URL, { useNewUrlParser: true });
    error.ifError("Connect to mongodb failed").thenStopExecution();

    databaseClient = client;
    defaultDatabase = client.db(constants.database.name);

    // set up indexes
    defaultDatabase.collection(constants.database.collections.users).createIndex({ location: "2dsphere" });
}

module.exports = {
    setupDatabase,
    getDatabase() {
        return defaultDatabase;
    }
};