const express = require("express");

class MiddlewareGroup {
    constructor() {
        this.middlewareGroup = [];
    }

    registerSupportForExpressApp(app) {
        this.app = app;

        // setup extended function (group) on Express app object
        app.group = this._groupRoutes;

        // attach middleware to dynamically check routes for grouped middleware
        app.use(this._scanRequestForApplicableMiddleware);
    }

    _groupRoutes(middleware, registerRoutes) {
        const router = express.Router();

        // call registerRoutes function to populate the routes that this middleware apply to
        registerRoutes(router);

        // record routes regex
        const routesRegex = [];
        for (const layer of router.stack) {
            if (layer.route) {
                routesRegex.push({
                    regex: new RegExp(layer.regexp),
                    methods: Object.getOwnPropertyNames(layer.route.methods)
                });
            }
        }

        // record middleware for routes and store in memory
        const applyMiddleware = {
            paths: routesRegex,
            apply: middleware
        };
        this.middlewareGroup.push(applyMiddleware);

        this.app.use(router);
    }

    _scanRequestForApplicableMiddleware(req, res, next) {
        // for each registered middleware
        for (const middleware of this.middlewareGroup) {
            // check if any routes belonging to middleware matches this request
            for (const path of middleware.paths) {
                const pathMatch = path.regex.test(req.path);
                if (pathMatch && this._middlewareContainsMethod(path.methods, req.method)) {
                    middleware.apply(req, res, next);
                    return;
                }
            }
        }
        next();
    }

    _middlewareContainsMethod(supportedMethods, requestMethod) {
        return supportedMethods.some(method => {
            return method.toLowerCase() === requestMethod.toLowerCase() || method.toLowerCase() === "_all";
        });
    }
}

module.exports = MiddlewareGroup;