const {bindTo, execSilentCallback} = require('silent-promise')
const FirebaseAuth = require('firebaseauth')
const constants = require('../utils/constants')
const {verifyToken} = require('../controllers/user/token')

const firebaseServiceAccountJson = require('../seclot-firebase.json')
const tokenMiddleware = FirebaseAuth.initTokenMiddleware(
  firebaseServiceAccountJson,
  tokenMiddlewareCallback
)

function tokenMiddlewareCallback(req, res, next, error, data) {
  if (error === 'ERROR_NO_TOKEN') {
    // token not supplied
    res.is.badRequest(401, 'Authorization token not provided')
  } else if (error === 'ERROR_INVALID_TOKEN') {
    // token failed verification
    res.is.badRequest(401, 'Unauthorized access. Token invalid or expired')
  } else if (error) {
    // some other error occurred (this should never happen!)
    res.is.serverError('Unexpected error')
  } else {
    // data contains user id and token (v0.2.0 and later) and full user information (id, displayName, email etc) for v0.1.1 and earlier

    req.user = data
    next()
  }
}

async function adminMiddleware(req, res, next) {
  const db = require('../config/db').getDatabase()

  const findById = {_id: req.user.id}
  const adminCollection = db.collection(constants.database.collections.admins)
  const [findAdminError, admin] = await execSilentCallback(
    adminCollection.findOne,
    bindTo(adminCollection)
  )(findById)
  findAdminError
    .ifError('failed to get admin info from database')
    .thenStopExecution()

  if (!admin) {
    res.is.badRequest(403, "You're NOT an admin")
  } else {
    req.admin = admin
    req.admin.id = req.admin._id
    delete req.admin._id
    next()
  }
}

async function activeAdminMiddleware(req, res, next) {
  const db = require('../config/db').getDatabase()

  const findById = {_id: req.user.id}
  const adminCollection = db.collection(constants.database.collections.admins)
  const [findAdminError, admin] = await execSilentCallback(
    adminCollection.findOne,
    bindTo(adminCollection)
  )(findById)
  findAdminError
    .ifError('failed to get admin info from database')
    .thenStopExecution()

  if (!admin) {
    res.is.badRequest(403, "You're NOT an admin")
  } else if (!admin.active) {
    res.is.badRequest(
      403,
      'Admin must set new password before performing this action'
    )
  } else {
    req.admin = admin
    req.admin.id = req.admin._id
    delete req.admin._id
    next()
  }
}

async function organizationMiddleware(req, res, next) {
  const db = require('../config/db').getDatabase()

  const findById = {_id: req.user.id}
  const options = {
    projection: {beneficiaries: false, disabledBeneficiaries: false},
  }
  const orgCollection = db.collection(
    constants.database.collections.organizations
  )
  const [findError, organization] = await execSilentCallback(
    orgCollection.findOne,
    bindTo(orgCollection)
  )(findById, options)
  findError
    .ifError('failed to get organization info from database')
    .thenStopExecution()

  if (!organization) {
    res.is.badRequest(403, 'This is NOT an Organization account')
  } else {
    req.organization = organization
    req.organization.id = req.organization._id
    delete req.organization._id
    next()
  }
}

function requireAuthCode(req, res, next) {
  if (!req.body.authCode) {
    res.is.badRequest(401, 'authCode is required')
    return
  }
  if (typeof req.body.authCode !== 'string') {
    res.is.badRequest(400, 'authCode is invalid')
    return
  }

  const userInfo = verifyToken(req.body.authCode)
  if (!userInfo) {
    res.is.badRequest(401, 'Invalid token')
    return
  }

  // all we need at this point is user Id
  req.user = {
    id: userInfo.id,
  }
  next()
}

async function userMiddleware(req, res, next) {
  const token = req.headers.token || req.body.token
  if (!token) {
    res.is.badRequest(401, 'Authorization token is required')
    return
  }
  if (typeof token !== 'string') {
    res.is.badRequest(400, 'Authorization token is invalid')
    return
  }

  const userInfo = verifyToken(token)
  if (!userInfo) {
    res.is.badRequest(401, 'Invalid token')
    return
  }

  if (userInfo.privileges !== 'full') {
    res.is.badRequest(403, 'User must set PIN before performing this action')
    return
  }

  const db = require('../config/db').getDatabase()
  const usersCollection = db.collection(constants.database.collections.users)
  const query = {_id: userInfo.id}
  const options = {projection: {lastLocationUpdate: false}}
  const [findUserError, user] = await execSilentCallback(
    usersCollection.findOne,
    bindTo(usersCollection)
  )(query, options)
  findUserError
    .ifError('Failed to get user info from database')
    .thenStopExecution()

  if (!user) {
    res.is.badRequest(403, 'User account not found')
  } else {
    req.user = user
    req.user.location = user.location ? user.location.coordinates : {}
    req.user.id = req.user._id
    delete req.user._id
    next()
  }
}

module.exports = {
  tokenMiddleware,
  adminMiddleware,
  activeAdminMiddleware,
  organizationMiddleware,
  requireAuthCode,
  userMiddleware,
}
