const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const {join} = require('path');
const {mkdir} = require('fs');
const {homedir} = require('os');
const MiddlewareGroup = require('./middleware-group');
const utils = require('../utils');

module.exports = function setupServer(app) {
	// allow cors
	app.use(cors());

	// setup custom response methods
	app.use(setupCustomResponses);

	app.use(bodyParser.json({limit: '1mb'}));
	app.use(bodyParser.urlencoded({limit: '2mb', extended: true}));

	// serve images from upload path
	console.log(homedir());
	const path = join(homedir(), process.env.UPLOADS_FOLDER);
	mkdir(path, '0744', err => {
		if (err && err.code !== 'EEXIST') {
			console.log(`Error creating directory: ${path}`, err);
		}
	});
	app.use('/image', express.static(path));

	// serve html files
	app.use('/', express.static(join(__dirname, '../public')));

	// expose upload path to other functions as an environment variable
	process.env.UPLOAD_PATH = path;

	// add ability to apply middleware to group of routes
	const middlewareGroup = new MiddlewareGroup();
	utils.bindMethodsToObject(middlewareGroup, MiddlewareGroup);
	middlewareGroup.registerSupportForExpressApp(app);
};

// noinspection JSUnusedLocalSymbols
function setupCustomResponses(req, res, next) {
	res.is = {
		ok: data => {
			sendResponse(200, data, res);
		},

		pdf: pdfDoc => {
			let pdfChunks = [];
			pdfDoc.on('data', chunk => {
				pdfChunks.push(chunk);
			});

			pdfDoc.on('end', () => {
				const pdfBytes = Buffer.concat(pdfChunks);
				res.setHeader('Content-Type', 'application/pdf');
				res.send(pdfBytes);
			});

			pdfDoc.end();
		},

		badRequest: (code, message) => {
			let responseCode = code;
			if (code < 400 || code >= 500) {
				responseCode = 400;
			}
			sendResponse(responseCode, {code, message}, res);
		},

		serverError: message => {
			sendResponse(500, message, res);
		}
	};

	next();
}

function sendResponse(code, data, res) {
	if (!res.beforeSendResponse) {
		res.status(code).send(data);
	} else {
		res.beforeSendResponse(() => res.status(code).send(data));
	}
}
